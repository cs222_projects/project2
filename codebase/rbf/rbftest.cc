#include <iostream>
#include <fstream>
#include <string>
#include <cassert>
#include <sys/stat.h>
#include <stdlib.h> 
#include <string.h>
#include <stdexcept>
#include <stdio.h> 

#include "pfm.h"
#include "rbfm.h"

using namespace std;

const int success = 0;
unsigned total = 0;

// Check if a file exists
bool FileExists(string fileName)
{
    struct stat stFileInfo;

    if(stat(fileName.c_str(), &stFileInfo) == 0) return true;
    else return false;
}

// Function to prepare the data in the correct form to be inserted/read
void prepareRecord(const int nameLength, const string &name, const int age, const float height, const int salary, void *buffer, int *recordSize)
{
    int offset = 0;

    memcpy((char *)buffer + offset, &nameLength, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)buffer + offset, name.c_str(), nameLength);
    offset += nameLength;

    memcpy((char *)buffer + offset, &age, sizeof(int));
    offset += sizeof(int);

    memcpy((char *)buffer + offset, &height, sizeof(float));
    offset += sizeof(float);

    memcpy((char *)buffer + offset, &salary, sizeof(int));
    offset += sizeof(int);

    *recordSize = offset;
}

void prepareLargeRecord(const int index, void *buffer, int *size)
{
    int offset = 0;

    // compute the count
    int count = index % 50 + 1;

    // compute the letter
    char text = index % 26 + 97;

    for(int i = 0; i < 10; i++)
    {
        memcpy((char *)buffer + offset, &count, sizeof(int));
        offset += sizeof(int);

        for(int j = 0; j < count; j++)
        {
            memcpy((char *)buffer + offset, &text, 1);
            offset += 1;
        }

        // compute the integer
        memcpy((char *)buffer + offset, &index, sizeof(int));
        offset += sizeof(int);

        // compute the floating number
        float real = (float)(index + 1);
        memcpy((char *)buffer + offset, &real, sizeof(float));
        offset += sizeof(float);
    }
    *size = offset;
}

void createRecordDescriptor(vector<Attribute> &recordDescriptor) {

    Attribute attr;
    attr.name = "EmpName";
    attr.type = TypeVarChar;
    attr.length = (AttrLength)100;
    recordDescriptor.push_back(attr);

    attr.name = "Age";
    attr.type = TypeInt;
    attr.length = (AttrLength)4;
    recordDescriptor.push_back(attr);

    attr.name = "Height";
    attr.type = TypeReal;
    attr.length = (AttrLength)4;
    recordDescriptor.push_back(attr);

    attr.name = "Salary";
    attr.type = TypeInt;
    attr.length = (AttrLength)4;
    recordDescriptor.push_back(attr);

}

void createLargeRecordDescriptor(vector<Attribute> &recordDescriptor)
{
    int index = 0;
    char *suffix = (char *)malloc(10);
    for(int i = 0; i < 10; i++)
    {
        Attribute attr;
        sprintf(suffix, "%d", index);
        attr.name = "attr";
        attr.name += suffix;
        attr.type = TypeVarChar;
        attr.length = (AttrLength)50;
        recordDescriptor.push_back(attr);
        index++;

        sprintf(suffix, "%d", index);
        attr.name = "attr";
        attr.name += suffix;
        attr.type = TypeInt;
        attr.length = (AttrLength)4;
        recordDescriptor.push_back(attr);
        index++;

        sprintf(suffix, "%d", index);
        attr.name = "attr";
        attr.name += suffix;
        attr.type = TypeReal;
        attr.length = (AttrLength)4;
        recordDescriptor.push_back(attr);
        index++;
    }
    free(suffix);
}

void createLargeRecordDescriptor2(vector<Attribute> &recordDescriptor)
{
    int index = 0;
    char *suffix = (char *)malloc(10);
    for(int i = 0; i < 10; i++)
    {
        Attribute attr;
        sprintf(suffix, "%d", index);
        attr.name = "attr";
        attr.name += suffix;
        attr.type = TypeVarChar;
        attr.length = (AttrLength)2000;
        recordDescriptor.push_back(attr);
        index++;

        sprintf(suffix, "%d", index);
        attr.name = "attr";
        attr.name += suffix;
        attr.type = TypeInt;
        attr.length = (AttrLength)4;
        recordDescriptor.push_back(attr);
        index++;

        sprintf(suffix, "%d", index);
        attr.name = "attr";
        attr.name += suffix;
        attr.type = TypeReal;
        attr.length = (AttrLength)4;
        recordDescriptor.push_back(attr);
        index++;
    }
    free(suffix);
}

bool compareFileSizes(string fileName1, string fileName2) {
    streampos s1, s2;
    ifstream in1(fileName1.c_str(), ifstream::in |ifstream::binary);
    in1.seekg(0, ifstream::end);
    s1 = in1.tellg();

    ifstream in2(fileName2.c_str(), ifstream::in |ifstream::binary);
    in2.seekg(0, ifstream::end);
    s2 = in2.tellg();

    cout << "File 1 size: " << s1 << endl;
    cout << "File 2 size: " << s2 << endl;

    if (s1 != s2) {
        return false;
    }
    return true;
}
ifstream::pos_type filesize(const char* filename)
{
    std::ifstream in(filename, std::ifstream::in | std::ifstream::binary);
    in.seekg(0, std::ifstream::end);
    cout << filename << " - file size:" << in.tellg() << endl;
    return in.tellg();
}

int RBFTest_1(PagedFileManager *pfm)
{
    // Functions Tested:
    // 1. Create File
    cout << "****In RBF Test Case 1****" << endl;

    RC rc;
    string fileName = "test";

    // Create a file named "test"
    rc = pfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 1 Failed!" << endl;
        return -1;
    }

    // Create "test" again, should fail
    rc = pfm->createFile(fileName.c_str());
    if(rc == success) {
        return -1;
    }
    assert(rc != success);

    cout << "Test Case 1 Passed!" << endl << endl;
    return 0;
}


int RBFTest_2(PagedFileManager *pfm)
{
    // Functions Tested:
    // 1. Destroy File
    cout << "****In RBF Test Case 2****" << endl;

    RC rc;
    string fileName = "test";

    rc = pfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(!FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been destroyed." << endl << endl;
        cout << "Test Case 2 Passed!" << endl << endl;
        return 0;
    }
    else
    {
        cout << "Failed to destroy file!" << endl;
        cout << "Test Case 2 Failed!" << endl << endl;
        return -1;
    }
}


int RBFTest_3(PagedFileManager *pfm)
{
    // Functions Tested:
    // 1. Create File
    // 2. Open File
    // 3. Get Number Of Pages
    // 4. Close File
    cout << "****In RBF Test Case 3****" << endl;

    RC rc;
    string fileName = "test_1";

    // Create a file named "test_1"
    rc = pfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 3 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_1"
    FileHandle fileHandle;
    rc = pfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }

    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Get the number of pages in the test file
    unsigned count = fileHandle.getNumberOfPages();
    if(count != (unsigned)0) {
        return -1;
    }
    assert(count == (unsigned)0);

    // Close the file "test_1"
    rc = pfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    cout << "Test Case 3 Passed!" << endl << endl;

    return 0;
}



int RBFTest_4(PagedFileManager *pfm)
{
    // Functions Tested:
    // 1. Open File
    // 2. Append Page
    // 3. Get Number Of Pages
    // 3. Close File
    cout << "****In RBF Test Case 4****" << endl;

    RC rc;
    string fileName = "test_1";

    // Open the file "test_1"
    FileHandle fileHandle;
    rc = pfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Append the first page
    void *data = malloc(PAGE_SIZE);
    for(unsigned i = 0; i < PAGE_SIZE; i++)
    {
        *((char *)data+i) = i % 94 + 32;
    }
    rc = fileHandle.appendPage(data);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Get the number of pages
    unsigned count = fileHandle.getNumberOfPages();

    printf("count: %d\n", count);
    if(count != (unsigned)1) {
        return -1;
    }
    assert(count == (unsigned)1);

    // Close the file "test_1"
    rc = pfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(data);

	int fsize = filesize(fileName.c_str());
	if (fsize == 0) {
		cout << "File Size should not be zero at this moment." << endl;
		return -1;
	}
    cout << "Test Case 4 Passed!" << endl << endl;

    return 0;
}


int RBFTest_5(PagedFileManager *pfm)
{
    // Functions Tested:
    // 1. Open File
    // 2. Read Page
    // 3. Close File
    cout << "****In RBF Test Case 5****" << endl;

    RC rc;
    string fileName = "test_1";

    // Open the file "test_1"
    FileHandle fileHandle;
    rc = pfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Read the first page
    void *buffer = malloc(PAGE_SIZE);
    rc = fileHandle.readPage(0, buffer);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Check the integrity of the page
    void *data = malloc(PAGE_SIZE);
    for(unsigned i = 0; i < PAGE_SIZE; i++)
    {
        *((char *)data+i) = i % 94 + 32;
    }
    rc = memcmp(data, buffer, PAGE_SIZE);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Close the file "test_1"
    rc = pfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(data);
    free(buffer);

    cout << "Test Case 5 Passed!" << endl << endl;

    return 0;
}


int RBFTest_6(PagedFileManager *pfm)
{
    // Functions Tested:
    // 1. Open File
    // 2. Write Page
    // 3. Read Page
    // 4. Close File
    // 5. Destroy File
    cout << "****In RBF Test Case 6****" << endl;

    RC rc;
    string fileName = "test_1";

    // Open the file "test_1"
    FileHandle fileHandle;
    rc = pfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Update the first page
    void *data = malloc(PAGE_SIZE);
    for(unsigned i = 0; i < PAGE_SIZE; i++)
    {
        *((char *)data+i) = i % 10 + 32;
    }
    rc = fileHandle.writePage(0, data);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Read the page
    void *buffer = malloc(PAGE_SIZE);
    rc = fileHandle.readPage(0, buffer);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Check the integrity
    rc = memcmp(data, buffer, PAGE_SIZE);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Close the file "test_1"
    rc = pfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(data);
    free(buffer);

	int fsize = filesize(fileName.c_str());
	if (fsize == 0) {
		cout << "File Size should not be zero at this moment." << endl;
		return -1;
	}
	
    // Destroy File
    rc = pfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(!FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been destroyed." << endl;
        cout << "Test Case 6 Passed!" << endl << endl;
        return 0;
    }
    else
    {
        cout << "Failed to destroy file!" << endl;
        cout << "Test Case 6 Failed!" << endl << endl;
        return -1;
    }
}


int RBFTest_7(PagedFileManager *pfm)
{
    // Functions Tested:
    // 1. Create File
    // 2. Open File
    // 3. Append Page
    // 4. Get Number Of Pages
    // 5. Read Page
    // 6. Write Page
    // 7. Close File
    // 8. Destroy File
    cout << "****In RBF Test Case 7****" << endl;

    RC rc;
    string fileName = "test_2";

    // Create the file named "test_2"
    rc = pfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 7 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_2"
    FileHandle fileHandle;
    rc = pfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Append 50 pages
    void *data = malloc(PAGE_SIZE);
    for(unsigned j = 0; j < 50; j++)
    {
        for(unsigned i = 0; i < PAGE_SIZE; i++)
        {
            *((char *)data+i) = i % (j+1) + 32;
        }
        rc = fileHandle.appendPage(data);
        if(rc != success) {
            return -1;
        }
        assert(rc == success);
    }
    cout << "50 Pages have been successfully appended!" << endl;

    // Get the number of pages
    unsigned count = fileHandle.getNumberOfPages();
    if(count != (unsigned)50) {
        return -1;
    }
    assert(count == (unsigned)50);

    // Read the 25th page and check integrity
    void *buffer = malloc(PAGE_SIZE);
    rc = fileHandle.readPage(24, buffer);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    for(unsigned i = 0; i < PAGE_SIZE; i++)
    {
        *((char *)data + i) = i % 25 + 32;
    }
    rc = memcmp(buffer, data, PAGE_SIZE);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);
    cout << "The data in 25th page is correct!" << endl;

    // Update the 25th page
    for(unsigned i = 0; i < PAGE_SIZE; i++)
    {
        *((char *)data+i) = i % 60 + 32;
    }
    rc = fileHandle.writePage(24, data);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Read the 25th page and check integrity
    rc = fileHandle.readPage(24, buffer);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    rc = memcmp(buffer, data, PAGE_SIZE);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Close the file "test_2"
    rc = pfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

	int fsize = filesize(fileName.c_str());
	if (fsize == 0) {
		cout << "File Size should not be zero at this moment." << endl;
		return -1;
	}
	
    // Destroy File
    rc = pfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(data);
    free(buffer);

    if(!FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been destroyed." << endl;
        cout << "Test Case 7 Passed!" << endl << endl;
        return 0;
    }
    else
    {
        cout << "Failed to destroy file!" << endl;
        cout << "Test Case 7 Failed!" << endl << endl;
        return -1;
    }
}

int RBFTest_8(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Record
    // 4. Read Record
    // 5. Close Record-Based File
    // 6. Destroy Record-Based File
    cout << "****In RBF Test Case 8****" << endl;

    RC rc;
    string fileName = "test_3";

    // Create a file named "test_3"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 8 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_3"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid;
    int recordSize = 0;
    void *record = malloc(100);
    void *returnedData = malloc(100);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert a record into a file
    prepareRecord(6, "Peters", 24, 170.1, 5000, record, &recordSize);
    cout << "Insert Data:" << endl;
    rbfm->printRecord(recordDescriptor, record);

    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Given the rid, read the record from file
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    cout << "Returned Data:" << endl;
    rbfm->printRecord(recordDescriptor, returnedData);

    // Compare whether the two memory blocks are the same
    if(memcmp(record, returnedData, recordSize) != 0)
    {
       cout << "Test Case 8 Failed!" << endl << endl;
        free(record);
        free(returnedData);
        return -1;
    }

    // Close the file "test_3"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

	int fsize = filesize(fileName.c_str());
	if (fsize == 0) {
		cout << "File Size should not be zero at this moment." << endl;
		return -1;
	}
	
    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 8 Passed!" << endl << endl;

    return 0;
}

int RBFTest_9(RecordBasedFileManager *rbfm, vector<RID> &rids, vector<int> &sizes) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Multiple Records
    // 4. Close Record-Based File
    cout << "****In RBF Test Case 9****" << endl;

    RC rc;
    string fileName = "test_4";

    // Create a file named "test_4"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 9 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_4"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid;
    void *record = malloc(1000);
    int numRecords = 2000;

    vector<Attribute> recordDescriptor;
    createLargeRecordDescriptor(recordDescriptor);

    for(unsigned i = 0; i < recordDescriptor.size(); i++)
    {
        cout << "Attribute Name: " << recordDescriptor.at(i).name << " Type: " << (AttrType)recordDescriptor.at(i).type << " Length: " << recordDescriptor.at(i).length << endl;
    }

    // Insert 2000 records into file
    for(int i = 0; i < numRecords; i++)
    {
        // Test insert Record
        int size = 0;
        memset(record, 0, 1000);
        prepareLargeRecord(i, record, &size);

        rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
        if(rc != success) {
            return -1;
        }

        assert(rc == success);

        rids.push_back(rid);
        sizes.push_back(size);
    }
    // Close the file "test_4"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

	int fsize = filesize(fileName.c_str());
	if (fsize == 0) {
		cout << "File Size should not be zero at this moment." << endl;
		return -1;
	}
	
    free(record);
    cout << "Test Case 9 Passed!" << endl << endl;

    return 0;
}

int RBFTest_10(RecordBasedFileManager *rbfm, vector<RID> &rids, vector<int> &sizes) {
    // Functions tested
    // 1. Open Record-Based File
    // 2. Read Multiple Records
    // 3. Close Record-Based File
    // 4. Destroy Record-Based File
    cout << "****In RBF Test Case 10****" << endl;

    RC rc;
    string fileName = "test_4";

    // Open the file "test_4"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int numRecords = 2000;
    void *record = malloc(1000);
    void *returnedData = malloc(1000);

    vector<Attribute> recordDescriptor;
    createLargeRecordDescriptor(recordDescriptor);

    for(int i = 0; i < numRecords; i++)
    {
        memset(record, 0, 1000);
        memset(returnedData, 0, 1000);
        rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[i], returnedData);
        if(rc != success) {
            return -1;
        }
        assert(rc == success);

		if (i % 500 == 0) {
	        cout << "Returned Data: " << endl;
   		    rbfm->printRecord(recordDescriptor, returnedData);
            printf("\n");
		}
		

        int size = 0;
        prepareLargeRecord(i, record, &size);
        if(memcmp(returnedData, record, sizes[i]) != 0)
        {
            printf("Return Data %d ERROR.\nThe record should be: \n", i);
            rbfm->printRecord(recordDescriptor, record);

            cout << "Test Case 10 Failed!" << endl << endl;
            free(record);
            free(returnedData);
            return -1;
        }
    }

    // Close the file "test_4"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

	int fsize = filesize(fileName.c_str());
	if (fsize == 0) {
		cout << "File Size should not be zero at this moment." << endl;
		return -1;
	}
	
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(!FileExists(fileName.c_str())) {
        cout << "File " << fileName << " has been destroyed." << endl << endl;
        free(record);
        free(returnedData);
        cout << "Test Case 10 Passed!" << endl << endl;
        return 0;
    }
    else {
        cout << "Failed to destroy file!" << endl;
        cout << "Test Case 10 Failed!" << endl << endl;
        free(record);
        free(returnedData);
        return -1;
    }
}

int RBFTest_17(RecordBasedFileManager *rbfm, vector<RID> &rids, vector<int> &sizes) {
    // Functions tested
    // 1. Open Record-Based File
    // 2. Delete Multiple Records
    // 3. Close Record-Based File
    cout << "****In RBF Test Case 17****" << endl;

    RBFTest_9(rbfm, rids, sizes);

    RC rc;
    string fileName = "test_4";

    // Open the file "test_4"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int numRecords = 2000;
    void *record = malloc(1000);
    void *returnedData = malloc(1000);

    vector<Attribute> recordDescriptor;
    createLargeRecordDescriptor(recordDescriptor);

    int old_fsize = filesize(fileName.c_str());
    printf("file size: %d, pages: %d.\n", old_fsize, old_fsize/PAGE_SIZE);
    printf("Delete Records.\n");

    for(int i = 0; i < numRecords; i++)
    {
        memset(record, 0, 1000);
        memset(returnedData, 0, 1000);
        rc = rbfm->deleteRecord(fileHandle, recordDescriptor, rids[i]);
        if(rc != success) {
            return -1;
        }
        assert(rc == success);

    }

    printf("All records have been deleted.\n");

    // Close the file "test_4"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int new_fsize = filesize(fileName.c_str());
    printf("file size: %d, pages: %d.\n", new_fsize, new_fsize/PAGE_SIZE);

    if (old_fsize != new_fsize) {
        cout << "File Size should not changed at this moment." << endl;
        return -1;
    }
    
    free(record);
    free(returnedData);
    cout << "Test Case 17 Passed!" << endl << endl;
}

int RBFTest_18(RecordBasedFileManager *rbfm, vector<RID> &rids, vector<int> &sizes) {
    // Functions tested
    // 1. Open Record-Based File
    // 2. Delete All Records
    // 3. Close Record-Based File
    cout << "****In RBF Test Case 18****" << endl;

    RBFTest_9(rbfm, rids, sizes);

    RC rc;
    string fileName = "test_4";

    // Open the file "test_4"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int numRecords = 2000;
    void *record = malloc(1000);
    void *returnedData = malloc(1000);

    vector<Attribute> recordDescriptor;
    createLargeRecordDescriptor(recordDescriptor);

    int old_fsize = filesize(fileName.c_str());
    printf("file size: %d, pages: %d.\n", old_fsize, old_fsize/PAGE_SIZE);
    printf("Delete Records.\n");

    rc = rbfm->deleteRecords(fileHandle);

    printf("All records have been deleted.\n");

    // Close the file "test_4"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int new_fsize = filesize(fileName.c_str());
    printf("file size: %d, pages: %d.\n", new_fsize, new_fsize/PAGE_SIZE);

    if (old_fsize != new_fsize) {
        cout << "File Size should not changed at this moment." << endl;
        return -1;
    }
    
    free(record);
    free(returnedData);
    cout << "Test Case 18 Passed!" << endl << endl;
}

int RBFTest_19(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Record
    // 4. Read Record
    // 5. Update Record
    // 6. Close Record-Based File
    // 7. Destroy Record-Based File
    cout << "****In RBF Test Case 19****" << endl;

    RC rc;
    string fileName = "test_6";

    // Create a file named "test_3"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 19 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_3"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid;
    int recordSize = 0;
    void *record = malloc(100);
    void *returnedData = malloc(100);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert a record into a file
    prepareRecord(6, "Peters", 24, 170.1, 5000, record, &recordSize);
    cout << "Insert Data:" << endl;
    rbfm->printRecord(recordDescriptor, record);
    // printf("Length of record: %d\n", rbfm->getRecordLength(recordDescriptor, record));

    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    cout << "Read Data:" << endl;

    // Given the rid, read the record from file
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    cout << "Returned Data:" << endl;
    rbfm->printRecord(recordDescriptor, returnedData);
    // printf("Length of record: %d\n", rbfm->getRecordLength(recordDescriptor, record));

    // Compare whether the two memory blocks are the same
    if(memcmp(record, returnedData, recordSize) != 0)
    {
       cout << "Test Case 19 Failed!" << endl << endl;
        free(record);
        free(returnedData);
        return -1;
    }


    cout << "Update Data:" << endl;
    prepareRecord(8, "FCBayern", 25, 171.1, 6000, record, &recordSize);
    rc = rbfm->updateRecord(fileHandle, recordDescriptor, record, rid);

    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    cout << "Returned Data:" << endl;
    rbfm->printRecord(recordDescriptor, returnedData);
    // printf("Length of record: %d\n", rbfm->getRecordLength(recordDescriptor, record));

    // Close the file "test_3"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int fsize = filesize(fileName.c_str());
    if (fsize == 0) {
        cout << "File Size should not be zero at this moment." << endl;
        return -1;
    }
    
    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 19 Passed!" << endl << endl;

    return 0;
}

int RBFTest_20(RecordBasedFileManager *rbfm, vector<RID> &rids, vector<int> &sizes) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Multiple Records
    // 4. Read a record.
    // 5. Update a record by extending its size.
    // 6. Read the record again.
    // 7. Close Record-Based File
    cout << "****In RBF Test Case 20****" << endl;

    RC rc;
    string fileName = "test_7";

    // Create a file named "test_4"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 20 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_4"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid;
    void *record = malloc(1000);
    void *returnRecord = malloc(1000);
    int numRecords = 2000;

    vector<Attribute> recordDescriptor;
    createLargeRecordDescriptor(recordDescriptor);

    for(unsigned i = 0; i < recordDescriptor.size(); i++)
    {
        cout << "Attribute Name: " << recordDescriptor.at(i).name << " Type: " << (AttrType)recordDescriptor.at(i).type << " Length: " << recordDescriptor.at(i).length << endl;
    }

    // Insert 2000 records into file
    for(int i = 0; i < numRecords; i++)
    {
        // Test insert Record
        int size = 0;
        memset(record, 0, 1000);
        prepareLargeRecord(i, record, &size);

        rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
        if(rc != success) {
            return -1;
        }

        assert(rc == success);

        rids.push_back(rid);
        sizes.push_back(size);
    }

    printf("\n\n\n\nRead record 0.\n");
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[0], record);
    rc = rbfm->printRecord(recordDescriptor, record);

    printf("\n\n\n\nRead record 1234.\n");
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[1234], record);
    rc = rbfm->printRecord(recordDescriptor, record);
    printf("size 1234: %d\n", sizes[1234]);

    printf("\n\n\n\nUpdate record.\n");
    prepareLargeRecord(1234, record, &sizes[0]);
    rc = rbfm->updateRecord(fileHandle, recordDescriptor, record, rids[0]);
    printf("size 0: %d\n", sizes[0]);

    printf("\n\n\n\nRead record again.\n");
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[0], returnRecord);
    rc = rbfm->printRecord(recordDescriptor, returnRecord);

    printf("compare two records: %d.\n", memcmp(record, returnRecord, 470));

    // Close the file "test_4"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }

    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);
    
    free(record);
    cout << "Test Case 20 Passed!" << endl << endl;
}

int RBFTest_21(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Record
    // 4. Read Record
    // 5. Close Record-Based File
    // 6. Destroy Record-Based File
    cout << "****In RBF Test Case 21****" << endl;

    RC rc;
    string fileName = "test_21";

    // Create a file named "test_21"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 21 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_21"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid0;
    RID rid1;
    int recordSize = 0;
    void *record = malloc(1000);
    void *returnedData = malloc(1000);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert a record into a file
    cout << "Insert Data:" << endl;
    prepareRecord(6, "Peters", 24, 170.1, 5000, record, &recordSize);
    rbfm->printRecord(recordDescriptor, record);
    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid0);
    printf("recordSize: %d\n", recordSize);

    // Insert a record into a file
    cout << "Insert Data:" << endl;
    prepareRecord(6, "Oliver", 25, 270.1, 6000, record, &recordSize);
    rbfm->printRecord(recordDescriptor, record);
    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid1);
    printf("recordSize: %d\n", recordSize);


    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    //Update record 0.
    cout << "Update Data:" << endl;
    prepareRecord(60, "OliverOliverOliverOliverOliverOliverOliverOliverOliverOliver", 25, 270.1, 6000, record, &recordSize);
    printf("recordSize: %d\n", recordSize);
    rc = rbfm->updateRecord(fileHandle, recordDescriptor, record, rid0);
    printf("rid0.pageNum: %d, rid0.slotNum: %d \n", rid0.pageNum, rid0.slotNum);

    cout << "Read Data:" << endl;
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid0, returnedData);
    rbfm->printRecord(recordDescriptor, returnedData);

    cout << "Delete Data:" << endl;
    rc = rbfm->deleteRecord(fileHandle, recordDescriptor, rid0);


    // Given the rid, read the record from file
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Close the file "test_21"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int fsize = filesize(fileName.c_str());
    if (fsize == 0) {
        cout << "File Size should not be zero at this moment." << endl;
        return -1;
    }
    
    // Destroy File
    // rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 21 Passed!" << endl << endl;

    return 0;
}

int RBFTest_22(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Record
    // 4. Read Attributes
    // 5. Close Record-Based File
    // 6. Destroy Record-Based File
    cout << "****In RBF Test Case 22****" << endl;

    RC rc;
    string fileName = "test_22";

    // Create a file named "test_22"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 22 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_22"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid0;
    RID rid1;
    int recordSize = 0;
    void *record = malloc(1000);
    void *returnedData = malloc(1000);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert a record into a file
    cout << "Insert Data:" << endl;
    prepareRecord(6, "Peters", 24, 170.1, 5000, record, &recordSize);
    rbfm->printRecord(recordDescriptor, record);
    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid0);
    printf("recordSize: %d\n", recordSize);

    // Insert a record into a file
    cout << "Insert Data:" << endl;
    prepareRecord(6, "Oliver", 25, 270.1, 6000, record, &recordSize);
    rbfm->printRecord(recordDescriptor, record);
    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid1);
    printf("recordSize: %d\n", recordSize);


    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    //Update record 0.
    cout << "Update Data:" << endl;
    prepareRecord(60, "OliverOliverOliverOliverOliverOliverOliverOliverOliverOliver", 25, 270.1, 6000, record, &recordSize);
    printf("recordSize: %d\n", recordSize);
    rc = rbfm->updateRecord(fileHandle, recordDescriptor, record, rid0);
    printf("rid0.pageNum: %d, rid0.slotNum: %d \n", rid0.pageNum, rid0.slotNum);

    cout << "Read Data:" << endl;
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid0, returnedData);
    rbfm->printRecord(recordDescriptor, returnedData);

    
    cout << "Read Attributes:" << endl;
    void *attrData = malloc(1000);
    memset(attrData, 0, 1000);
    rc = rbfm->readAttribute(fileHandle, recordDescriptor, rid0, "EmpName", attrData);
    printf("EmpName: %s.\n", (char *)attrData + sizeof(int));
    
    memset(attrData, 0, 1000);
    rc = rbfm->readAttribute(fileHandle, recordDescriptor, rid0, "Age", attrData);
    printf("Age: %d.\n", *(int *)attrData);

    memset(attrData, 0, 1000);
    rc = rbfm->readAttribute(fileHandle, recordDescriptor, rid0, "Height", attrData);
    printf("Height: %f.\n", *(float *)attrData);

    memset(attrData, 0, 1000);
    rc = rbfm->readAttribute(fileHandle, recordDescriptor, rid0, "Salary", attrData);
    printf("Salary: %d.\n", *(int *)attrData);



    // Given the rid, read the record from file
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Close the file "test_22"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int fsize = filesize(fileName.c_str());
    if (fsize == 0) {
        cout << "File Size should not be zero at this moment." << endl;
        return -1;
    }
    
    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 22 Passed!" << endl << endl;

    return 0;
}

int RBFTest_23(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Records
    // 4. Scan Records
    // 5. Close Record-Based File
    // 6. Destroy Record-Based File
    cout << "****In RBF Test Case 23****" << endl;

    RC rc;
    string fileName = "test_23";

    // Create a file named "test_23"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 23 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_23"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid;
    int recordSize = 0;
    void *record = malloc(100);
    void *returnedData = malloc(100);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert records into a file
    cout << "Insert Data:" << endl;
    int numOfRecords = 10;
    for(int i=0; i<numOfRecords; i++)
    {
        if(i != 1)
        {
            prepareRecord(6, "Peters", i, 170.1 + (i/13.0 + i%11), 5000 + (i%50), record, &recordSize);
        }
        else
        {
            prepareRecord(6, "Oliver", i, 170.1 + (i/13.0 + i%11), 5000 + (i%50), record, &recordSize);
        }
        // rbfm->printRecord(recordDescriptor, record);

        rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
        if(rc != success) {
            return -1;
        }
        assert(rc == success);
    }

    // // Given the rid, read the record from file
    // rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
    // if(rc != success) {
    //     return -1;
    // }
    // assert(rc == success);

    // cout << "Returned Data:" << endl;
    // rbfm->printRecord(recordDescriptor, returnedData);

    // Close the file "test_23"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int fsize = filesize(fileName.c_str());
    if (fsize == 0) {
        cout << "File Size should not be zero at this moment." << endl;
        return -1;
    }


    
    cout << "Scan Data:" << endl;
    RBFM_ScanIterator rbfmsi;
    vector<string> attributeNames;
    attributeNames.push_back("EmpName");
    attributeNames.push_back("Age");
    void *compValue = malloc(100);

    // int age = 2;
    // memcpy(compValue, &age, sizeof(int));
    char *name = new char[6];
    int nameLength = 6;
    strcpy(name, "Oliver");
    printf("name: %s\n", name);
    memcpy(compValue, &nameLength, sizeof(int));
    memcpy((char*)compValue + sizeof(int), name, nameLength);

    // Open the file "test_23" AGAIN
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
    // rbfm->printRecord(recordDescriptor, returnedData);

    cout << "rbfm->scan:" << endl;
    rc = rbfm->scan(fileHandle, recordDescriptor, "EmpName", GT_OP, compValue, attributeNames,rbfmsi);
    // rc = rbfm->scan(fileHandle, recordDescriptor, "", NO_OP, NULL, attributeNames,rbfmsi);

    printf("rbfmsi.conditionAttributeType: %d\n", rbfmsi.conditionAttributeType);
    printf("rbfmsi.conditionAttributeId: %d\n", rbfmsi.conditionAttributeId);
    printf("rbfmsi.compOp: %d\n", rbfmsi.compOp);
    printf("*(rbfmsi.value): %d\n", *((int *)rbfmsi.value));
    printf("rbfmsi.attributeNames[0]: %s\n", rbfmsi.attributeNames[0].c_str());
    printf("rbfmsi.attributeNames[1]: %s\n", rbfmsi.attributeNames[1].c_str());
    printf("rbfmsi.pendingRID.pageNum: %d\n", rbfmsi.pendingRID.pageNum);
    printf("rbfmsi.pendingRID.slotNum: %d\n", rbfmsi.pendingRID.slotNum);

    int counter = 0;
    while(rbfmsi.getNextRecord(rid, returnedData) != RBFM_EOF){
       printf("EmpName: %s\n", (char *)returnedData + sizeof(int));
       printf("Age: %d\n", *((char *)returnedData + sizeof(int) + 6));
       //process the data;

       if(counter++ == 10)
       {
        break;
       }
    }

    rbfmsi.close();
    
    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 23 Passed!" << endl << endl;

    return 0;
}

int RBFTest_24(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Records
    // 4. Scan Records
    // 5. Close Record-Based File
    // 6. Destroy Record-Based File
    cout << "****In RBF Test Case 24****" << endl;

    RC rc;
    string fileName = "test_24";

    // Create a file named "test_24"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 24 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_24"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    vector<RID> rids;
    RID rid;
    RID rid0;
    int recordSize = 0;
    void *record = malloc(1000);
    void *returnedData = malloc(1000);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert records into a file
    cout << "Insert Data:" << endl;
    int numOfRecords = 10;
    for(int i=0; i<numOfRecords; i++)
    {

        if(i != 1)
        {
            prepareRecord(6, "Peters", i, 170.1 + (i/13.0 + i%11), 5000 + (i%50), record, &recordSize);
        }
        else
        {
            prepareRecord(6, "Oliver", i, 170.1 + (i/13.0 + i%11), 5000 + (i%50), record, &recordSize);
        }
        // rbfm->printRecord(recordDescriptor, record);

        rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
        rids.push_back(rid);

        if(rc != success) {
            return -1;
        }
        assert(rc == success);
    }

    //Update record 0.
    cout << "Update Data:" << endl;
    prepareRecord(60, "OliverOliverOliverOliverOliverOliverOliverOliverOliverOliver", 25, 270.1, 6000, record, &recordSize);
    printf("recordSize: %d\n", recordSize);
    rc = rbfm->updateRecord(fileHandle, recordDescriptor, record, rids[0]);

    cout << "Read Data:" << endl;
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[0], returnedData);
    rbfm->printRecord(recordDescriptor, returnedData);

    printf("Delete Records.\n");
    rc = rbfm->deleteRecord(fileHandle, recordDescriptor, rids[2]);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    cout << "Read Records:" << endl;
    for(int i=0; i<numOfRecords; i++)
    {
        if(rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[i], returnedData) == success)
        {
            rbfm->printRecord(recordDescriptor, returnedData);
        }
    }  

    cout << "reorganizePage:" << endl;
    rc = rbfm->reorganizePage(fileHandle, recordDescriptor, 0); 

    cout << "Read Records again:" << endl;
    for(int i=0; i<numOfRecords; i++)
    {
        if(rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[i], returnedData) == success)
        {
            rbfm->printRecord(recordDescriptor, returnedData);
        }
    }   


    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int fsize = filesize(fileName.c_str());
    if (fsize == 0) {
        cout << "File Size should not be zero at this moment." << endl;
        return -1;
    }


    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 24 Passed!" << endl << endl;

    return 0;
}

int RBFTest_25(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Records
    // 4. Delete Records
    // 5. Reorganize file
    // 6. Close Record-Based File
    // 7. Destroy Record-Based File
    cout << "****In RBF Test Case 25****" << endl;

    RC rc;
    string fileName = "test_25";

    // Create a file named "test_25"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 25 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_25"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid;
    int recordSize = 0;
    void *record = malloc(100);
    void *returnedData = malloc(100);
    int numOfRecords = 200;
    RID rids[numOfRecords];

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert a record into a file
    cout << "Insert Data:" << endl;

    for(int i=0; i<numOfRecords; i++)
    {
        prepareRecord(6, "Peters", i, 170.1 + (i/11.3), 5000 + (i%50), record, &recordSize);
        // rbfm->printRecord(recordDescriptor, record);

        rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rids[i]);
        // printf("rids[%d].pageNum:%d, slotNum:%d\n", i, rids[i].pageNum, rids[i].slotNum);
        if(rc != success) {
            return -1;
        }
        assert(rc == success);
    }

    cout << "Update Data:" << endl;
    prepareRecord(60, "OliverOliverOliverOliverOliverOliverOliverOliverOliverOliver", 150, 170.1, 5000, record, &recordSize);

    rc = rbfm->updateRecord(fileHandle, recordDescriptor, record, rids[150]);
    

    // Delete the first 100 records
    cout << "Delete Data:" << endl;
    for(int i = 0; i < numOfRecords/2; i++)
    {
        rc = rbfm->deleteRecord(fileHandle, recordDescriptor, rids[i]);
        assert(rc == success);

        rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[i], record);
        assert(rc != success);
    }
    cout << "After deletion!" << endl;

    rc = rbfm->reorganizeFile(fileHandle, recordDescriptor);
    assert(rc == success);

    for(int i = 0; i < numOfRecords/2; i++)
    {
        printf("rids[%d].pageNum:%d, slotNum:%d\n", i, rids[i].pageNum, rids[i].slotNum);
        rc = rbfm->readRecord(fileHandle, recordDescriptor, rids[i], returnedData);
        rbfm->printRecord(recordDescriptor, returnedData);
        assert(rc == success);

        // Print the tuple
    }

    // Close the file "test_25"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int fsize = filesize(fileName.c_str());
    if (fsize == 0) {
        cout << "File Size should not be zero at this moment." << endl;
        return -1;
    }
    
    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 25 Passed!" << endl << endl;

    return 0;
}

int RBFTest_26(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Record
    // 4. Delete Record
    // 5. Delete Record Again
    // 6. Close Record-Based File
    // 7. Destroy Record-Based File
    cout << "****In RBF Test Case 26****" << endl;

    RC rc;
    string fileName = "test_26";

    // Create a file named "test_26"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 26 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_26"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid;
    int recordSize = 0;
    void *record = malloc(100);
    void *returnedData = malloc(100);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert a record into a file
    prepareRecord(6, "Peters", 24, 170.1, 5000, record, &recordSize);
    cout << "Insert Data:" << endl;
    rbfm->printRecord(recordDescriptor, record);

    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Given the rid, read the record from file
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    cout << "Returned Data:" << endl;
    rbfm->printRecord(recordDescriptor, returnedData);

    // Compare whether the two memory blocks are the same
    if(memcmp(record, returnedData, recordSize) != 0)
    {
       cout << "Test Case 26 Failed!" << endl << endl;
        free(record);
        free(returnedData);
        return -1;
    }

    cout << "Delete Record:" << endl;
    rc = rbfm->deleteRecord(fileHandle, recordDescriptor, rid);
    if(rc != success) {
        return -1;
    }

    cout << "Delete Record Again:" << endl;
    rc = rbfm->deleteRecord(fileHandle, recordDescriptor, rid);
    if(rc == success) {
        cout << "Delete Record should not success." << endl;
        return -1;
    }

    // Close the file "test_26"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int fsize = filesize(fileName.c_str());
    if (fsize == 0) {
        cout << "File Size should not be zero at this moment." << endl;
        return -1;
    }
    
    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 26 Passed!" << endl << endl;

    return 0;
}

int RBFTest_27(RecordBasedFileManager *rbfm) {
    // Functions tested
    // 1. Create Record-Based File
    // 2. Open Record-Based File
    // 3. Insert Record
    // 4. Delete Record
    // 5. Update Record
    // 6. Close Record-Based File
    // 7. Destroy Record-Based File
    cout << "****In RBF Test Case 27****" << endl;

    RC rc;
    string fileName = "test_27";

    // Create a file named "test_27"
    rc = rbfm->createFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    if(FileExists(fileName.c_str()))
    {
        cout << "File " << fileName << " has been created." << endl;
    }
    else
    {
        cout << "Failed to create file!" << endl;
        cout << "Test Case 27 Failed!" << endl << endl;
        return -1;
    }

    // Open the file "test_27"
    FileHandle fileHandle;
    rc = rbfm->openFile(fileName.c_str(), fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);


    RID rid;
    int recordSize = 0;
    void *record = malloc(100);
    void *returnedData = malloc(100);

    vector<Attribute> recordDescriptor;
    createRecordDescriptor(recordDescriptor);

    // Insert a record into a file
    prepareRecord(6, "Peters", 24, 170.1, 5000, record, &recordSize);
    cout << "Insert Data:" << endl;
    rbfm->printRecord(recordDescriptor, record);

    rc = rbfm->insertRecord(fileHandle, recordDescriptor, record, rid);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    // Given the rid, read the record from file
    rc = rbfm->readRecord(fileHandle, recordDescriptor, rid, returnedData);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    cout << "Returned Data:" << endl;
    rbfm->printRecord(recordDescriptor, returnedData);

    // Compare whether the two memory blocks are the same
    if(memcmp(record, returnedData, recordSize) != 0)
    {
       cout << "Test Case 27 Failed!" << endl << endl;
        free(record);
        free(returnedData);
        return -1;
    }

    cout << "Delete Record:" << endl;
    rc = rbfm->deleteRecord(fileHandle, recordDescriptor, rid);
    if(rc != success) {
        return -1;
    }

    cout << "Update Record:" << endl;
    rc = rbfm->updateRecord(fileHandle, recordDescriptor, record, rid);
    if(rc == success) {
        cout << "Update Record should not success." << endl;
        return -1;
    }

    // Close the file "test_27"
    rc = rbfm->closeFile(fileHandle);
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    int fsize = filesize(fileName.c_str());
    if (fsize == 0) {
        cout << "File Size should not be zero at this moment." << endl;
        return -1;
    }
    
    // Destroy File
    rc = rbfm->destroyFile(fileName.c_str());
    if(rc != success) {
        return -1;
    }
    assert(rc == success);

    free(record);
    free(returnedData);
    cout << "Test Case 27 Passed!" << endl << endl;

    return 0;
}

int main()
{
    PagedFileManager *pfm = PagedFileManager::instance(); // To test the functionality of the paged file manager
    RecordBasedFileManager *rbfm = RecordBasedFileManager::instance(); // To test the functionality of the record-based file manager

    remove("test");
    remove("test_1");
    remove("test_2");
    remove("test_3");
    remove("test_4");
    remove("test_5");

    string failTest = "";

    int rc = RBFTest_1(pfm);
    if (rc == 0) {
        total += 3;
    } else {
    	failTest += "1 ";
    }
    rc = RBFTest_2(pfm);
    if (rc == 0) {
        total += 3;
    } else {
    	failTest += "2 ";
    }
    rc = RBFTest_3(pfm);
    if (rc == 0) {
        total += 4;
    } else {
    	failTest += "3 ";
    }
    rc = RBFTest_4(pfm);
    if (rc == 0) {
        total += 4;
    } else {
    	failTest += "4 ";
    }
    rc = RBFTest_5(pfm);
    if (rc == 0) {
        total += 4;
    } else {
    	failTest += "5 ";
    }
    rc = RBFTest_6(pfm);
    if (rc == 0) {
        total += 4;
    } else {
    	failTest += "6 ";
    }
    rc = RBFTest_7(pfm);
    if (rc == 0) {
        total += 4;
    } else {
    	failTest += "7 ";
    }
    rc = RBFTest_8(rbfm);
    if (rc == 0) {
        total += 4;
    } else {
        failTest += "8 ";
    }

    vector<RID> rids;
    vector<int> sizes;
    rc = RBFTest_9(rbfm, rids, sizes);
    if (rc == 0) {
        total += 5;
    }  else {
        failTest += "9 ";
    }
    rc = RBFTest_10(rbfm, rids, sizes);
    if (rc == 0) {
        total += 5;
    }  else {
        failTest += "10 ";
    }

    // rc = RBFTest_17(rbfm, rids, sizes);
    // rc = RBFTest_18(rbfm, rids, sizes);
    // rc = RBFTest_19(rbfm);
    rc = RBFTest_20(rbfm, rids, sizes);
    // rc = RBFTest_21(rbfm);
    rc = RBFTest_22(rbfm);
    rc = RBFTest_23(rbfm);
    rc = RBFTest_24(rbfm);
    // rc = RBFTest_25(rbfm);
    // rc = RBFTest_26(rbfm);
    // rc = RBFTest_27(rbfm);

    if (failTest.length() > 1) {
    	cout << "Failed test case(s): " + failTest << endl;
    } else {
    	cout << "No test case(s) failed." << endl;
    }
    cout << "Score for test #1 ~ #10 is: " << total << " / 40" << endl;
    return 0;
}
