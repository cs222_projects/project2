#include "rbfm.h"


///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//////////Class RecordBasedFileManager//////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

RecordBasedFileManager* RecordBasedFileManager::_rbf_manager = 0;

RecordBasedFileManager* RecordBasedFileManager::instance()
{
    if(!_rbf_manager)
        _rbf_manager = new RecordBasedFileManager();

    return _rbf_manager;
}

RecordBasedFileManager::RecordBasedFileManager()
{
}

RecordBasedFileManager::~RecordBasedFileManager()
{
}

///////////////////////////////////////////////////////////////////////////////////
//////////Files Operation Functions//////////
///////////////////////////////////////////////////////////////////////////////////

RC RecordBasedFileManager::createFile(const string &fileName) 
{
    const char *chFileName = fileName.c_str();
    return PagedFileManager::instance()->createFile(chFileName);
}

RC RecordBasedFileManager::destroyFile(const string &fileName) 
{
    const char *chFileName = fileName.c_str();
    return PagedFileManager::instance()->destroyFile(chFileName);
}

RC RecordBasedFileManager::openFile(const string &fileName, FileHandle &fileHandle) 
{
    const char *chFileName = fileName.c_str();
    return PagedFileManager::instance()->openFile(chFileName, fileHandle);
}

RC RecordBasedFileManager::closeFile(FileHandle &fileHandle) 
{
    return PagedFileManager::instance()->closeFile(fileHandle);
}

///////////////////////////////////////////////////////////////////////////////////
//////////Records Operation Functions//////////
///////////////////////////////////////////////////////////////////////////////////

RC RecordBasedFileManager::insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid) 
{
    if(recordDescriptor.size() == 0)
    {
        perror("Invalid recordDescriptor");
        assert(false);
        return -1;
    }

    short recordLength = getRecordLengthFromDescriptor(recordDescriptor, data) + sizeof(short);
    char occupiedSlots = (char)(recordLength / SLOT_SIZE) + 1;
    char firstSlot = findInsertLocation(fileHandle, occupiedSlots, rid);

    void *recordData = malloc(recordLength);
    memcpy(recordData, data, recordLength - sizeof(short));
    appendRecordHeader(recordDescriptor, recordData);

    void *pageData = malloc(PAGE_SIZE);
    fileHandle.readPage(rid.pageNum, pageData);
    memcpy((char *)pageData + firstSlot*SLOT_SIZE, (char *)recordData, recordLength);

    free(recordData);


    insertRID(firstSlot, occupiedSlots, pageData, rid);
    fileHandle.writePage(rid.pageNum, pageData);

    free(pageData);

    return 0;
}

// Update a record identified by the given rid. 
// Assume the rid does not change after update
RC RecordBasedFileManager::updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid)
{
    char *pageData = new char[PAGE_SIZE];

    if(fileHandle.readPage(rid.pageNum, pageData)<0)
    {
        perror("readRecord");
        fprintf(stderr, "Page %d does not exist.\n", rid.pageNum);
        return -1;
    }

    // Recalculate the length of the updated record.
    char firstSlot;
    short recordLength = getRecordLengthFromDescriptor(recordDescriptor, data) + sizeof(short);
    char new_occupiedSlots = (char)(recordLength / SLOT_SIZE) + 1;
    char old_cccupiedSlots;

    resolveRID(&firstSlot, &old_cccupiedSlots, pageData, rid);

    if(old_cccupiedSlots == 0)
    {
        // perror("updateRecord");
        // fprintf(stderr, "Record does not exist.\n");

        free(pageData);
        return -1;
    }


    if(new_occupiedSlots <= old_cccupiedSlots)
    {
        void *recordData = malloc(recordLength);
        memcpy(recordData, data, recordLength - sizeof(short));
        appendRecordHeader(recordDescriptor, recordData);
        
        updateRecordAtCurrentPosition(firstSlot, old_cccupiedSlots, pageData, recordData, recordLength);
        
        free(recordData);

        setRIDInfo(firstSlot, new_occupiedSlots, pageData, rid);
    }
    else
    {
        migrateRecord(fileHandle, recordDescriptor, firstSlot, new_occupiedSlots, pageData, data, rid);
    }


    fileHandle.writePage(rid.pageNum, pageData);
    free(pageData);
    
    return 0;
}

RC RecordBasedFileManager::updateRecordAtCurrentPosition(char firstSlot, char occupiedSlots, void *pageData, const void *data, int recordLength)
{
    unsigned offset = firstSlot*SLOT_SIZE;
    unsigned slotLength = occupiedSlots*SLOT_SIZE;
    memset((char *)pageData + offset, 0, slotLength);
    memcpy((char *)pageData + offset, (char *)data, recordLength);

    return 0;
}

RC RecordBasedFileManager::migrateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, char firstSlot, char occupiedSlots, void *pageData, const void *data, const RID &rid)
{
    RID migratedRid;

    insertRecord(fileHandle, recordDescriptor, data, migratedRid);

    unsigned migratedPageNum = migratedRid.pageNum;
    unsigned migratedSlotNum = migratedRid.slotNum;

    vector<Attribute> recordDescriptorForTombstone;
    createRecordDescriptorForTombstone(recordDescriptorForTombstone);

    void *recordForTombstone = malloc(2*sizeof(unsigned));
    prepareRecordForTombstone(migratedPageNum, migratedSlotNum, recordForTombstone);

    updateRecord(fileHandle, recordDescriptorForTombstone, recordForTombstone, rid);

    //Set the hightest bit of occupiedSlots to 1
    occupiedSlots = 1 | 0x80;  //1000 0000

    //IMPORTANT: page should be read from file again, since updateRecord might have overwritten it.
    fileHandle.readPage(rid.pageNum, pageData);
    setRIDInfo(firstSlot, occupiedSlots, pageData, rid);

    return 0;

}

RC RecordBasedFileManager::readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data) 
{
    void *pageData = malloc(PAGE_SIZE);

    if(fileHandle.readPage(rid.pageNum, pageData)<0)
    {
        perror("readRecord");
        fprintf(stderr, "Page %d does not exist.\n", rid.pageNum);
        return -1;
    }

    bool iterated;
    if(readRecordOnPage(recordDescriptor, rid, &iterated, pageData, data) == -1)
    {
        free(pageData);
        return -1;
    }

    free(pageData);

    if(iterated == true)
    {
        readMigratedRecord(fileHandle, recordDescriptor, data);
    }

    return 0;
}

RC RecordBasedFileManager::readRecordOnPage(const vector<Attribute> &recordDescriptor, const RID &rid, bool *iterated, const void *pageData, void *data)
{
    char firstSlot;
    char occupiedSlots;
    *iterated = resolveRID(&firstSlot, &occupiedSlots, pageData, rid);

    if(occupiedSlots == 0)
    {
        //record is not existed.
        return -1;
    }

    char *recordData = (char *)pageData + firstSlot*SLOT_SIZE;

    memcpy(data, recordData, getRecordLengthFromHeader(recordData) + sizeof(short));

    removeRecordHeader(data);

    return 0;
}

RC RecordBasedFileManager::readMigratedRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, void *data)
{
    RID migratedRid;
    vector<Attribute> recordDescriptorForTombstone;

    createRecordDescriptorForTombstone(recordDescriptorForTombstone);
    getTombstoneFromRecord(migratedRid, recordDescriptorForTombstone, data);

    readRecord(fileHandle, recordDescriptor, migratedRid, data);

    return 0;
}

RC RecordBasedFileManager::deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid)
{
    char *pageData = new char[PAGE_SIZE];

    if(fileHandle.readPage(rid.pageNum, pageData)<0)
    {
        // perror("deleteRecord");
        // fprintf(stderr, "Page %d does not exist.\n", rid.pageNum);

        free(pageData);
        return -1;
    }

    char firstSlot;
    char occupiedSlots;
    bool iterated = resolveRID(&firstSlot, &occupiedSlots, pageData, rid);

    if(occupiedSlots == 0)
    {
        // perror("deleteRecord");
        // fprintf(stderr, "Record does not exist.\n");

        free(pageData);
        return -1;
    }

    RID migratedRid;

    if(iterated == true)
    {
        char *tombstoneData = (char *)(pageData + firstSlot*SLOT_SIZE);
        getMigratedRID(fileHandle, tombstoneData, migratedRid);
    }

    memset(pageData+firstSlot*SLOT_SIZE, 0, occupiedSlots*SLOT_SIZE);

    firstSlot = 0;
    occupiedSlots = 0;
    setRIDInfo(firstSlot, occupiedSlots, pageData, rid);

    fileHandle.writePage(rid.pageNum, pageData);
    free(pageData);

    if(iterated == true)
    {
        deleteRecord(fileHandle, recordDescriptor, migratedRid);
    }

    return 0;
}

RC RecordBasedFileManager::getMigratedRID(FileHandle &fileHandle, void *tombstoneData, RID &migratedRid)
{
    vector<Attribute> recordDescriptorForTombstone;

    createRecordDescriptorForTombstone(recordDescriptorForTombstone);
    getTombstoneFromRecord(migratedRid, recordDescriptorForTombstone, tombstoneData);

    return 0;
}

// Delete all records in a file. 
RC RecordBasedFileManager::deleteRecords(FileHandle &fileHandle)
{
    fileHandle.initializeFile();

    return 0;
}

RC RecordBasedFileManager::printRecord(const vector<Attribute> &recordDescriptor, const void *data) 
{
    if(recordDescriptor.size() == 0)
    {
        return -1;
    }
    int offset = 0;

    char *varChar;
    for (unsigned int i=0; i<recordDescriptor.size(); i++)
    {
        printf("%s: ", recordDescriptor[i].name.c_str());

        switch (recordDescriptor[i].type)
        {
            case TypeInt:
                printf("(Interger) %d\t", getIntData(offset, data));
                offset += 4;
                break;

            case TypeReal:
                printf("(Real) %f\t",getRealData(offset, data));
                offset += 4;
                break;

            case TypeVarChar:
                int varCharLength;
                memcpy(&varCharLength, (char *)data + offset, sizeof(int));
                offset += 4;
                varChar = new char[MAX_ATTRIBUTE_SIZE];
                getVarCharData(offset, data, varChar, varCharLength);
                printf("(VarChar - Length) %d\t", varCharLength);
                printf("(VarChar - Content) %s\t", varChar);
                offset += varCharLength;
                free(varChar);
                break;

            default:
                return -1;
        }
    }
    printf("\n");

    return 0;
}

RC RecordBasedFileManager::appendRecordHeader(const vector<Attribute> &recordDescriptor, void *data)
{
    void *temp = malloc(MAX_RECORD_SIZE);
    short recordLength = getRecordLengthFromDescriptor(recordDescriptor, data);
    
    memcpy(temp, &recordLength, sizeof(short));
    memcpy((char *)temp + sizeof(short), data, recordLength);
    memcpy(data, temp, sizeof(short) + recordLength);

    free(temp);

    return 0;
}

RC RecordBasedFileManager::removeRecordHeader(void *data)
{
    void *temp = malloc(MAX_RECORD_SIZE);
    short recordLength = getRecordLengthFromHeader(data);
    
    memcpy(temp, (char *)data + sizeof(short), recordLength);
    memcpy(data, temp, recordLength);
    memset((char *)data + recordLength, 0, sizeof(short));

    free(temp);

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
//////////Records Resolution Functions//////////
///////////////////////////////////////////////////////////////////////////////////

int RecordBasedFileManager::getIntData(int offset, const void* data)
{
    int intData;    
    memcpy(&intData, (char *)data + offset, sizeof(int));
    return intData;
}
float RecordBasedFileManager::getRealData(int offset, const void* data)
{
    float realData;
    memcpy(&realData, (char *)data + offset, sizeof(float));
    return realData;
}
RC RecordBasedFileManager::getVarCharData(int offset, const void* data, char* varChar, int varCharLength)
{
    memcpy((void *)varChar, (char *)data + offset, varCharLength);
    varChar[varCharLength] = '\0';
    return 0;
}

short RecordBasedFileManager::getRecordLengthFromDescriptor(const vector<Attribute> &recordDescriptor, const void *data)
{
    unsigned offset = 0;
    for(unsigned int i=0; i<recordDescriptor.size(); i++)
    {
        passOneAttribute(&offset, recordDescriptor[i], data);
    }

    return offset;
}

short RecordBasedFileManager::getRecordLengthFromHeader(const void *data)
{
    return *(short *)data;
}

//Given a record descriptor, read a specific attribute of a record identified by a given rid. 
RC RecordBasedFileManager::readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string attributeName, void *data)
{
    void *recordData = malloc(MAX_RECORD_SIZE);
    readRecord(fileHandle, recordDescriptor, rid, recordData);

    RC rc = readAttributeFromRecord(recordDescriptor, attributeName, recordData, data);

    free(recordData);
    return (rc!=-1)?0:-1;
}

RC RecordBasedFileManager::readAttributeFromRecord(const vector<Attribute> &recordDescriptor, const string attributeName, const void *recordData, void *attributeData)
{
    unsigned offset = 0;

    for (unsigned i=0; i<recordDescriptor.size(); i++)
    {
        if(recordDescriptor[i].name != attributeName)
        {
            passOneAttribute(&offset, recordDescriptor[i], recordData);
        }
        else
        {
            readAttributeData(offset, recordDescriptor[i], recordData, attributeData);

            return i;
        }
    }

    return -1;
}

RC RecordBasedFileManager::passOneAttribute(unsigned *offset, const Attribute attr, const void *recordData)
{
    if (attr.type == TypeVarChar)
    {
        int varCharLength;
        memcpy(&varCharLength, (char *)recordData + *offset, sizeof(int));
        *offset += varCharLength;
    }
    *offset += 4;

    return 0;
}

RC RecordBasedFileManager::readAttributeData(unsigned offset, const Attribute attr, const void *recordData, void *data)
{
    if (attr.type != TypeVarChar)
    {
        memcpy(data, (char *)recordData + offset, sizeof(int));
    }
    else
    {
        int varCharLength;
        memcpy(&varCharLength, (char *)recordData + offset, sizeof(int));

        memcpy(data, (char *)recordData + offset, varCharLength + sizeof(int));
    }

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
//////////Slots Information Related Functions//////////
///////////////////////////////////////////////////////////////////////////////////

char RecordBasedFileManager::findInsertLocation(FileHandle &fileHandle, char occupiedSlots, RID &rid)
{
    unsigned metaPageNum = -1;
    unsigned totalPages = fileHandle.getNumberOfPages();
    unsigned formalTotalPages = totalPages;
    char firstSlot;
    char *metaPage = new char[PAGE_SIZE];

    for(unsigned i=0; ; i++)
    {
        //  Load next meta page.
        if((i % PAGES_PER_SEGMENT) == 0)
        {
            metaPageNum++;
            
            //If the meta page is not existed.
            if((i == totalPages) && (totalPages != 0))
            {
                fileHandle.insertMetaPage(metaPageNum);
            }

            fileHandle.getMetaPage(metaPageNum, metaPage);
        }

        // If reach the end of the file, create a new page.
        if(i == totalPages)
        {
            fileHandle.initializePage(i);
            totalPages++;
        }

        firstSlot = getFirstAvailableSlot(i, metaPage);

        if(firstSlot + occupiedSlots < NUM_OF_SLOTS)
        {
            rid.pageNum = i;
            setFirstAvailableSlot(rid.pageNum, metaPage, firstSlot + occupiedSlots);
            fileHandle.updateMetaPage(metaPageNum, metaPage);

            if(totalPages != formalTotalPages)
            {
                fileHandle.setNumberOfPages(totalPages);
            }
            
            break;
        }
    }

    free(metaPage);

    return firstSlot;
}

char RecordBasedFileManager::getFirstAvailableSlot(PageNum pageNum, const void *data)
{
    char result;
    unsigned offset = pageNum%PAGES_PER_SEGMENT;
    memcpy(&result, (char *)data+offset, sizeof(char));
    return result;
}

RC RecordBasedFileManager::setFirstAvailableSlot(PageNum pageNum, void *data, char slotNum)
{
    unsigned offset = pageNum%PAGES_PER_SEGMENT;
    memcpy((char *)data+offset, &slotNum, sizeof(char));

    return 0;
}


///////////////////////////////////////////////////////////////////////////////////
//////////RID Operation Functions//////////
///////////////////////////////////////////////////////////////////////////////////

RC RecordBasedFileManager::insertRID(char firstSlot, char occupiedSlots, void *data, RID &rid)

{

    rid.slotNum = getFirstIdleRID(data);


    setRIDInfo(firstSlot, occupiedSlots, data, rid);


    return 0;

}

//Get firstSlot and occupiedSlots from a rid, return true if the rid need to be iterated.
bool RecordBasedFileManager::resolveRID(char *firstSlot, char *occupiedSlots, const void *data, const RID &rid)
{
    int offset = PAGE_SIZE - 2*(PAGE_SIZE / SLOT_SIZE) + 2*rid.slotNum;

    memcpy(firstSlot, (char *)data+offset, sizeof(char));
    memcpy(occupiedSlots, (char *)data+offset+1, sizeof(char));

    if(*occupiedSlots < 0)
    {
        *occupiedSlots &= 0x7f;  //0111 1111
        return true;
    }
    else
    {
        return false;
    }
}

char RecordBasedFileManager::getFirstIdleRID(const void *data)

{

    int offset = PAGE_SIZE - 2*(PAGE_SIZE / SLOT_SIZE);

    char occupiedRID;

    char location;


    for(location=0; location<(PAGE_SIZE / SLOT_SIZE - 8); location++)

    {

        occupiedRID = *((char *)data + offset + 2*location + 1);

        if(occupiedRID == 0)

        {

            break;

        }

    }


    return location;

}

RC RecordBasedFileManager::setRIDInfo(char firstSlot, char occupiedSlots, void *data, const RID &rid)
{
    int offset = PAGE_SIZE - 2*(PAGE_SIZE / SLOT_SIZE) + 2*rid.slotNum;

    memcpy((char *)data+offset, &firstSlot, sizeof(char));
    memcpy((char *)data+offset+1, &occupiedSlots, sizeof(char));

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
//////////Page Reorganization Functions//////////
///////////////////////////////////////////////////////////////////////////////////

RC RecordBasedFileManager::reorganizePage(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const unsigned pageNumber)
{
    // Load page
    void *pageData = malloc(PAGE_SIZE);
    fileHandle.readPage(pageNumber, pageData);

    //Map the usage of the page by rid.
    char *usage = new char[NUM_OF_SLOTS];
    memset(usage, 0xff, NUM_OF_SLOTS);
    vector<RID> rids;

    generateUsageArray(pageNumber, rids, pageData, usage);

    //Shift up data and update slots information.
    shiftRecord(rids, pageData, usage);

    fileHandle.writePage(pageNumber, pageData);

    free(usage);

    return 0;
}

RC RecordBasedFileManager::generateUsageArray(const unsigned pageNumber, vector<RID> &rids, const void *pageData, char *usage)
{
    int offset = PAGE_SIZE - 2*(PAGE_SIZE / SLOT_SIZE);
    char validRID;
    char ridIterator = 0;

    char firstSlot;
    char occupiedSlots;

    for(char slotNum=0; slotNum<NUM_OF_SLOTS; slotNum++)
    {
        validRID = *((char *)pageData + offset + 2*slotNum + 1);
        if(validRID != 0)
        {
            RID rid;
            rid.pageNum = pageNumber;
            rid.slotNum = slotNum;
            rids.push_back(rid);

            resolveRID(&firstSlot, &occupiedSlots, pageData, rid);

            //fill the array
            memset((char *)usage+firstSlot, ridIterator, occupiedSlots);
            
            ridIterator++;
        }
    }

    return 0;
}

RC RecordBasedFileManager::shiftRecord(vector<RID> &rids, void *pageData, char *usage)
{
    char idleSegmentStart = -1;
    bool discoveredIdleSegment = false;
    char firstSlot;
    char occupiedSlots;

    for(char i=0; i<NUM_OF_SLOTS;)
    {
        if(usage[(int)i] == -1)
        {
            if(discoveredIdleSegment == false)
            {
                //Just enter an idle segment
                discoveredIdleSegment = true;
                idleSegmentStart = i;

            }

            i++;
        }
        else
        {
            discoveredIdleSegment = false;

            if(idleSegmentStart != -1)
            {
                RID rid = rids[usage[(int)i]];
                resolveRID(&firstSlot, &occupiedSlots, pageData, rid);

                moveRecordData(firstSlot, occupiedSlots, idleSegmentStart, pageData);

                updateUsageArray(i, occupiedSlots, idleSegmentStart, usage);

                setRIDInfo(idleSegmentStart, occupiedSlots, pageData, rid);

                i = idleSegmentStart + occupiedSlots;
            }
            else
            {
                i++;
            }
            
        }
    }

    return 0;
}

RC RecordBasedFileManager::moveRecordData(char firstSlot, char occupiedSlots, char idleSegmentStart, void *pageData)
{
    //move data
    void *tempRecord = malloc(occupiedSlots*SLOT_SIZE);
    memcpy(tempRecord, (char *)pageData + firstSlot*SLOT_SIZE, occupiedSlots*SLOT_SIZE);
    memcpy((char *)pageData + idleSegmentStart*SLOT_SIZE, tempRecord, occupiedSlots*SLOT_SIZE);
    free(tempRecord);

    //erase old data
    memset((char *)pageData + (idleSegmentStart + occupiedSlots)*SLOT_SIZE, 0, (firstSlot - idleSegmentStart)*SLOT_SIZE);

    return 0;
}

RC RecordBasedFileManager::updateUsageArray(char recordStart, char occupiedSlots, char idleSegmentStart, char *usage)
{
    //update usage array
    void *tempUsage = malloc(occupiedSlots);
    memcpy(tempUsage, usage + recordStart, occupiedSlots);
    memcpy(usage + idleSegmentStart, tempUsage, occupiedSlots);
    free(tempUsage);

    //erase old occupied information
    memset(usage + idleSegmentStart + occupiedSlots, 0xff, recordStart - idleSegmentStart);

    return 0;
}

RC RecordBasedFileManager::reorganizeFile(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor)
{
    unsigned numOfPage = fileHandle.getNumberOfPages();

    if(numOfPage == 0)
    {
        return 0;
    }

    queue<char *> records;

    PageNum insertPageNum = 0;
    PageNum loadPageNum = 0;
    retriveAllRecordsOnPage(fileHandle, recordDescriptor, insertPageNum, records);

    while(((loadPageNum == 0) || (records.size() == 0)) && (loadPageNum < numOfPage - 1))
    {
        loadPageNum++;
        retriveAllRecordsOnPage(fileHandle, recordDescriptor, loadPageNum, records);
    }

    RID rid;
    while(true)
    {
        if(records.size() != 0)
        {
            insertRecordsInQueue(fileHandle, recordDescriptor, records, rid);
        }

        if((rid.pageNum > insertPageNum) || (records.size() == 0))
        {
            insertPageNum = rid.pageNum;
            loadPageNum++;
            if(loadPageNum == numOfPage)
            {
                break;
            }
            retriveAllRecordsOnPage(fileHandle, recordDescriptor, loadPageNum, records);
        }
    }

    insertAllRecords(fileHandle, recordDescriptor, records);

    return 0;
}

RC RecordBasedFileManager::retriveAllRecordsOnPage(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, PageNum pageNum, queue<char *> &records)
{
    void *loadPageData = malloc(PAGE_SIZE);
    fileHandle.readPage(pageNum, loadPageData);
    void *recordData;
    RID rid;

    rid.pageNum = pageNum;
    for(rid.slotNum = 0; rid.slotNum < NUM_OF_SLOTS; rid.slotNum++)
    {
        recordData = malloc(MAX_RECORD_SIZE);
        bool iterated;
        if(readRecordOnPage(recordDescriptor, rid, &iterated, loadPageData, recordData) == 0)
        {
            if(iterated == true)
            {
                readMigratedRecord(fileHandle, recordDescriptor, recordData);
                
                //Only migrated records need to be deleted, others are initialized.
                deleteRecord(fileHandle, recordDescriptor, rid);
            }

            records.push((char *)recordData);
        }
    }
    free(loadPageData);

    fileHandle.initializePage(pageNum);

    void *metaPage = malloc(PAGE_SIZE);
    unsigned metaPageNum = (unsigned)(pageNum/PAGES_PER_SEGMENT);

    fileHandle.getMetaPage(metaPageNum, metaPage);
    setFirstAvailableSlot(pageNum, metaPage, 0);
    fileHandle.updateMetaPage(metaPageNum, metaPage);

    free(metaPage);

    return 0;
}

RC RecordBasedFileManager::insertAllRecords(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, queue<char *> &records)
{
    RID rid;
    while(records.size()!=0)
    {
        insertRecordsInQueue(fileHandle, recordDescriptor, records, rid);
    }

    return 0;
}

RC RecordBasedFileManager::insertRecordsInQueue(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, queue<char *> &records, RID &rid)
{
    insertRecord(fileHandle, recordDescriptor, records.front(), rid);
    free(records.front());
    records.pop();

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
//////////Tombstones Operation Functions//////////
///////////////////////////////////////////////////////////////////////////////////

RC RecordBasedFileManager::createRecordDescriptorForTombstone(vector<Attribute> &recordDescriptor) {

    Attribute attr;

    attr.name = "pageNum";
    attr.type = TypeInt;
    attr.length = (AttrLength)4;
    recordDescriptor.push_back(attr);

    attr.name = "slotNum";
    attr.type = TypeInt;
    attr.length = (AttrLength)4;
    recordDescriptor.push_back(attr);

    return 0;

}

RC RecordBasedFileManager::prepareRecordForTombstone(const unsigned pageNum, const unsigned slotNum, void *buffer)
{
    int offset = 0;

    memcpy((char *)buffer, &pageNum, sizeof(unsigned));
    offset += sizeof(unsigned);

    memcpy((char *)buffer + offset, &slotNum, sizeof(unsigned));

    return 0;
}

RC RecordBasedFileManager::getTombstoneFromRecord(RID &rid, const vector<Attribute> &recordDescriptor, const void *record)
{
    int offset = 0;

    memcpy(&(rid.pageNum), (char *)record, sizeof(unsigned));
    offset += sizeof(unsigned);

    memcpy(&(rid.slotNum), (char *)record + offset, sizeof(unsigned));

    return 0;
}

///////////////////////////////////////////////////////////////////////////////////
//////////Scan Functions//////////
///////////////////////////////////////////////////////////////////////////////////

//Scan returns an iterator to allow the caller to go through the results one by one. 
//Given a record descriptor, scan a file, i.e., sequentially read all the entries in the file. A scan has a filter condition associated with it, e.g., it consists of a list of attributes to project out as well as a predicate on an attribute ("Sal > 40000"). Specifically, the parameter conditionAttribute here is the attribute's name that you are going to apply the filter on. The compOp parameter is the comparison type that is going to be used in the filtering process. The value parameter is the value of the conditionAttribute that is going to be used to filter out records. Note that the retrieved records should only have the fields that are listed in the vector attributeNames. Please take a look at the test cases for more information on how to use this method. 
RC RecordBasedFileManager::scan(FileHandle &fileHandle,
      const vector<Attribute> &recordDescriptor,
      const string &conditionAttribute,
      const CompOp compOp,                  // comparision type such as "<" and "="
      const void *value,                    // used in the comparison
      const vector<string> &attributeNames, // a list of projected attributes
      RBFM_ScanIterator &rbfm_ScanIterator)
{
    rbfm_ScanIterator.fileHandle = fileHandle;
    rbfm_ScanIterator.recordDescriptor = recordDescriptor;
    rbfm_ScanIterator.compOp = compOp;
    rbfm_ScanIterator.attributeNames = attributeNames;

    getConditionAttribute(recordDescriptor, conditionAttribute, rbfm_ScanIterator);

    if(compOp != NO_OP)
    {
        getConditionValue(rbfm_ScanIterator, value);
    }

    rbfm_ScanIterator.pendingRID.pageNum = 0;
    rbfm_ScanIterator.pendingRID.slotNum = 0;

    return 0;
}

RC RecordBasedFileManager::getConditionAttribute(const vector<Attribute> &recordDescriptor, const string &conditionAttribute, RBFM_ScanIterator &rbfm_ScanIterator)
{
    //Deal with empty conditionAttribute
    if(conditionAttribute == "")
    {
        rbfm_ScanIterator.conditionAttributeId = -2;
        return 0;
    }

    rbfm_ScanIterator.conditionAttributeId = -1;

    for (int i=0; i<(int)recordDescriptor.size(); i++)
    {
        if(recordDescriptor[i].name == conditionAttribute)
        {
            rbfm_ScanIterator.conditionAttributeId = i;
            rbfm_ScanIterator.conditionAttributeType = recordDescriptor[i].type;

            break;
        }
    }

    return 0;
}

RC RecordBasedFileManager::getConditionValue(RBFM_ScanIterator &rbfm_ScanIterator, const void *value)
{
    if(rbfm_ScanIterator.conditionAttributeType != TypeVarChar)
    {
        rbfm_ScanIterator.value = malloc(sizeof(int));
        memcpy(rbfm_ScanIterator.value, value, sizeof(int));
    }
    else
    {
        rbfm_ScanIterator.value = malloc(*(int *)value + 1);
        memcpy(rbfm_ScanIterator.value, (char *)value + sizeof(int), *(int *)value);
        memset((char *)rbfm_ScanIterator.value + *(int *)value, 0, 1);
    }

    return 0;
}


///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
//////////Class RBFM_ScanIterator//////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////////////

RBFM_ScanIterator::RBFM_ScanIterator()
{
}

RBFM_ScanIterator::~RBFM_ScanIterator()
{
}

RC RBFM_ScanIterator::getNextRecord(RID &rid, void *data)
{
    RC returnVal = 0;
    //Read pending page
    void *pageData = malloc(PAGE_SIZE);
    void *recordData = malloc(MAX_RECORD_SIZE);
    memset(recordData, 0, MAX_RECORD_SIZE);

    if(fileHandle.readPage(pendingRID.pageNum, pageData) < 0)
    {
        free(recordData);
        free(pageData);
        return RBFM_EOF;
    }

    while(validateNextRecord(pageData, recordData) == false)
    {
        gotoNextRID(rid);

        if(pendingRID.slotNum == NUM_OF_SLOTS)
        {
            pendingRID.pageNum++;

            if(pendingRID.pageNum == fileHandle.getNumberOfPages())
            {
                returnVal = RBFM_EOF;
                break;
            }
            pendingRID.slotNum = 0;
            fileHandle.readPage(pendingRID.pageNum, pageData);
        }
        
    }

    if(returnVal != RBFM_EOF)
    {
        retrieveRecord(recordData, data);
        gotoNextRID(rid);
    }

    free(recordData);
    free(pageData);

    return returnVal;
}

bool RBFM_ScanIterator::validateNextRecord(void *pageData, void *recordData)
{
    if(validRID(pageData) == false)
    {
        return false;
    }
    
    RecordBasedFileManager *rbfm=RecordBasedFileManager::instance();
    bool forReadRecordOnly;
    rbfm->readRecordOnPage(recordDescriptor, pendingRID, &forReadRecordOnly, pageData, recordData);

    return isSatisfied(recordData);
}

bool RBFM_ScanIterator::validRID(const void *pageData)
{
    int offset = PAGE_SIZE - 2*(PAGE_SIZE / SLOT_SIZE) + 2*pendingRID.slotNum;
    char firstSlot;
    char occupiedSlots;

    memcpy(&firstSlot, (char *)pageData+offset, sizeof(char));
    memcpy(&occupiedSlots, (char *)pageData+offset+1, sizeof(char));

    if((occupiedSlots < 0) || (occupiedSlots == 0))  //0111 1111
    {
        return false;
    }
    else
    {
        return true;
    }
}

bool RBFM_ScanIterator::isSatisfied(const void *recordData)
{
    void *compData = malloc(MAX_RECORD_SIZE);
    
    getCompData(recordData, compData);

    bool compResult = compAttribute(compData);

    free(compData);

    return compResult;
}

RC RBFM_ScanIterator::getCompData(const void *recordData, void *compData)
{
    unsigned offset = 0;
    RecordBasedFileManager *rbfm=RecordBasedFileManager::instance();

    for (int i=0; i<(int)recordDescriptor.size(); i++)
    {
        if(i != conditionAttributeId)
        {
            rbfm->passOneAttribute(&offset, recordDescriptor[i], recordData);
        }
        else
        {
            rbfm->readAttributeData(offset, recordDescriptor[i], recordData, compData);
            if(recordDescriptor[i].type == TypeVarChar)
            {
                int varCharLength = *(int *)compData;
                memcpy((char *)compData, (char *)compData + sizeof(int), varCharLength);
                memset((char *)compData + varCharLength, 0, sizeof(int));
            }

            break;
        }
    }

    return 0;
}

bool RBFM_ScanIterator::compAttribute(const void *compData)
{
    if((conditionAttributeId == -2) || (compOp == NO_OP))
    {
        return true;
    }

    if(conditionAttributeType == TypeInt)
    {
        return compNumValue<int>(*(int *)compData, *(int *)value, compOp);
    }

    if(conditionAttributeType == TypeReal)
    {
        return compNumValue<float>(*(float *)compData, *(float *)value, compOp);
    }

    if(conditionAttributeType == TypeVarChar)
    {
        return compStrValue((char *)compData, (char *)value, compOp);
    }

    return false;
}

RC RBFM_ScanIterator::gotoNextRID(RID &currentRID)
{
    currentRID.pageNum = pendingRID.pageNum;
    currentRID.slotNum = pendingRID.slotNum;
    pendingRID.slotNum++;

    return 0;
}

RC RBFM_ScanIterator::retrieveRecord(const void *recordData, void *retrivedData)
{
    unsigned recordOffset = 0;
    unsigned retrivedOffset = 0;
    unsigned old_recordOffset;
    RecordBasedFileManager *rbfm=RecordBasedFileManager::instance();

    for(unsigned i=0; i<attributeNames.size(); i++)
    {
        recordOffset = 0;
        for (unsigned j=0; j<recordDescriptor.size(); j++)
        {
            if(attributeNames[i] == recordDescriptor[j].name)
            {
                rbfm->readAttributeData(recordOffset, recordDescriptor[j], recordData, (char *)retrivedData + retrivedOffset);

                old_recordOffset = recordOffset;
                rbfm->passOneAttribute(&recordOffset, recordDescriptor[j], recordData);
                retrivedOffset += (recordOffset - old_recordOffset);
                break;
            }
            else
            {
                rbfm->passOneAttribute(&recordOffset, recordDescriptor[j], recordData);
            }
        }
    }

    return 0;
}

RC RBFM_ScanIterator::close()
{
    PagedFileManager::instance()->closeFile(fileHandle);
    if(compOp != NO_OP)
    {
        free(value);
    }

    return 0;
}

template <class T>
bool compNumValue(T a, T b, CompOp compOp)
{
    if(compOp == EQ_OP)
        return a == b;

    if(compOp == LT_OP)
        return a < b;

    if(compOp == GT_OP)
        return a > b;

    if(compOp == LE_OP)
        return a <= b;

    if(compOp == GE_OP)
        return a >= b;

    if(compOp == NE_OP)
        return a != b;

    if(compOp == NO_OP)
        return true;
    
    return false;
}

bool compStrValue(const char *keyData, const char *compData, CompOp compOp)
{
    if(compOp == EQ_OP)
        return (strcmp(keyData, compData) == 0);

    if(compOp == LT_OP)
        return (strcmp(keyData, compData) < 0);

    if(compOp == GT_OP)
        return (strcmp(keyData, compData) > 0);

    if(compOp == LE_OP)
        return (strcmp(keyData, compData) <= 0);

    if(compOp == GE_OP)
        return (strcmp(keyData, compData) >= 0);

    if(compOp == NE_OP)
        return (strcmp(keyData, compData) != 0);

    if(compOp == NO_OP)
        return true;
    
    return false;
}
