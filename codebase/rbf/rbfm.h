#ifndef _rbfm_h_
#define _rbfm_h_

#include <string>
#include <vector>
#include <queue> 

#include "../rbf/pfm.h"

#define SLOT_SIZE 32
#define NUM_OF_SLOTS (PAGE_SIZE / SLOT_SIZE - 8)   /* =120, 8=PAGE_SIZE/SLOT_SIZE*2/SLOT_SIZE */

#define MAX_RECORD_SIZE 2048
#define MAX_ATTRIBUTE_SIZE MAX_RECORD_SIZE

using namespace std;


// Record ID
typedef struct
{
  unsigned pageNum;
  unsigned slotNum;
} RID;


// Attribute
typedef enum { TypeInt = 0, TypeReal, TypeVarChar } AttrType;

typedef unsigned AttrLength;

struct Attribute {
    string   name;     // attribute name
    AttrType type;     // attribute type
    AttrLength length; // attribute length
};

// Comparison Operator (NOT needed for part 1 of the project)
typedef enum { EQ_OP = 0,  // =
           LT_OP,      // <
           GT_OP,      // >
           LE_OP,      // <=
           GE_OP,      // >=
           NE_OP,      // !=
           NO_OP       // no condition
} CompOp;



/****************************************************************************
The scan iterator is NOT required to be implemented for part 1 of the project 
*****************************************************************************/

# define RBFM_EOF (-1)  // end of a scan operator

// RBFM_ScanIterator is an iteratr to go through records
// The way to use it is like the following:
//  RBFM_ScanIterator rbfmScanIterator;
//  rbfm.open(..., rbfmScanIterator);
//  while (rbfmScanIterator(rid, data) != RBFM_EOF) {
//    process the data;
//  }
//  rbfmScanIterator.close();


class RBFM_ScanIterator {
public:
  RBFM_ScanIterator();
  ~RBFM_ScanIterator();

  FileHandle fileHandle;

  vector<Attribute> recordDescriptor;

  int conditionAttributeId;
  AttrType conditionAttributeType;

  CompOp compOp;                  // comparision type such as "<" and "="
  void *value;                    // used in the comparison
  vector<string> attributeNames; // a list of projected attributes

  RID pendingRID;

  // "data" follows the same format as RecordBasedFileManager::insertRecord()
  RC getNextRecord(RID &rid, void *data);
  RC close();

private:
  bool validateNextRecord(void *pageData, void *recordData);
  bool validRID(const void *pageData);
  bool isSatisfied(const void *recordData);
  RC gotoNextRID(RID &currentRID);
  RC retrieveRecord(const void *recordData, void *retrivedData);

  RC getCompData(const void *recordData, void *compData);
  bool compAttribute(const void *compData);
};


class RecordBasedFileManager
{
public:
  static RecordBasedFileManager* instance();

  RC createFile(const string &fileName);
  
  RC destroyFile(const string &fileName);
  
  RC openFile(const string &fileName, FileHandle &fileHandle);
  
  RC closeFile(FileHandle &fileHandle);

  //  Format of the data passed into the function is the following:
  //  1) data is a concatenation of values of the attributes
  //  2) For int and real: use 4 bytes to store the value;
  //     For varchar: use 4 bytes to store the length of characters, then store the actual characters.
  //  !!!The same format is used for updateRecord(), the returned data of readRecord(), and readAttribute()
  RC insertRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, RID &rid);

  RC readRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, void *data);
  
  RC readRecordOnPage(const vector<Attribute> &recordDescriptor, const RID &rid, bool *iterated, const void *pageData, void *data);

  // This method will be mainly used for debugging/testing
  RC printRecord(const vector<Attribute> &recordDescriptor, const void *data);

/**************************************************************************************************************************************************************
***************************************************************************************************************************************************************
IMPORTANT, PLEASE READ: All methods below this comment (other than the constructor and destructor) are NOT required to be implemented for part 1 of the project
***************************************************************************************************************************************************************
***************************************************************************************************************************************************************/
  // Delete all records in a file. 
  RC deleteRecords(FileHandle &fileHandle);

  // Delete a record identified by the given rid. 
  RC deleteRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid);

  // Update a record identified by the given rid. 
  // Assume the rid does not change after update
  RC updateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const void *data, const RID &rid);

  short getRecordLengthFromDescriptor(const vector<Attribute> &recordDescriptor, const void *data);
  short getRecordLengthFromHeader(const void *data);


  RC appendRecordHeader(const vector<Attribute> &recordDescriptor, void *data);
  RC removeRecordHeader(void *data);

  // Read an attribute given its name and the rid. 
  RC readAttribute(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const RID &rid, const string attributeName, void *data);
  RC readAttributeFromRecord(const vector<Attribute> &recordDescriptor, const string attributeName, const void *recordData, void *data);

  RC passOneAttribute(unsigned *offset, const Attribute attr, const void *recordData);
  RC readAttributeData(unsigned offset, const Attribute attr, const void *recordData, void *data);

  // Push the free space towards the end of the page. 
  RC reorganizePage(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, const unsigned pageNumber);

  // scan returns an iterator to allow the caller to go through the results one by one. 
  RC scan(FileHandle &fileHandle,
      const vector<Attribute> &recordDescriptor,
      const string &conditionAttribute,
      const CompOp compOp,                  // comparision type such as "<" and "="
      const void *value,                    // used in the comparison
      const vector<string> &attributeNames, // a list of projected attributes
      RBFM_ScanIterator &rbfm_ScanIterator);


// Extra credit for part 2 of the project, please ignore for part 1 of the project
public:
  // Reorganize the records in the file such that the records are collected towards the beginning of the file. 
  RC reorganizeFile(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor);


protected:
  RecordBasedFileManager();
  ~RecordBasedFileManager();

private:
  static RecordBasedFileManager *_rbf_manager;

  RC updateRecordAtCurrentPosition(char firstSlot, char occupiedSlots, void *pageData, const void *data, int recordLength);
  RC migrateRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, char firstSlot, char occupiedSlots, void *pageData, const void *data, const RID &rid);
  RC readMigratedRecord(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, void *data);

  RC retriveAllRecordsOnPage(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, PageNum pageNum, queue<char *> &records);
  RC insertAllRecords(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, queue<char *> &records);
  RC insertRecordsInQueue(FileHandle &fileHandle, const vector<Attribute> &recordDescriptor, queue<char *> &records, RID &rid);

  int getIntData(int offset, const void* data);
  float getRealData(int offset, const void* data);
  RC getVarCharData(int offset, const void* data, char* varChar, int varCharLength);

  char findInsertLocation(FileHandle &fileHandle, char occupiedSlots, RID &rid);

  char getFirstAvailableSlot(PageNum pageNum, const void *data);
  RC setFirstAvailableSlot(PageNum pageNum, void *data, char slotNum);

  RC insertRID(char slotNum, char occupiedSlots, void *data, RID &rid);
  char getFirstIdleRID(const void *data);
  bool resolveRID(char *firstSlot, char *occupiedSlots, const void *data, const RID &rid);
  RC setRIDInfo(char firstSlot, char occupiedSlots, void *data, const RID &rid);
  RC getMigratedRID(FileHandle &fileHandle, void *tombstoneData, RID &migratedRid);

  RC createRecordDescriptorForTombstone(vector<Attribute> &recordDescriptor);
  RC prepareRecordForTombstone(const unsigned pageNum, const unsigned slotNum, void *buffer);
  RC getTombstoneFromRecord(RID &rid, const vector<Attribute> &recordDescriptor, const void *record);

  RC generateUsageArray(const unsigned pageNumber, vector<RID> &rids, const void *pageData, char *usage);
  RC shiftRecord(vector<RID> &rids, void *pageData, char *usage);
  RC moveRecordData(char firstSlot, char occupiedSlots, char idleSegmentStart, void *pageData);
  RC updateUsageArray(char firstSlot, char occupiedSlots, char idleSegmentStart, char *usage);

  RC getConditionAttribute(const vector<Attribute> &recordDescriptor, const string &conditionAttribute, RBFM_ScanIterator &rbfm_ScanIterator);
  RC getConditionValue(RBFM_ScanIterator &rbfm_ScanIterator, const void *value);
};
  
template <class T>
bool compNumValue(T a, T b, CompOp compOp);

bool compStrValue(const char *compData, const char *value, CompOp compOp);


#endif
