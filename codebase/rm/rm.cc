
#include "rm.h"

RelationManager* RelationManager::_rm = 0;

RelationManager* RelationManager::instance()
{
    if(!_rm)
        _rm = new RelationManager();

    return _rm;
}

RelationManager::RelationManager()
{
    _rbfm = RecordBasedFileManager::instance();
    FileHandle fileHandle;
    if(_rbfm->openFile(TABLE_FILE_NAME, fileHandle) != 0 || _rbfm->openFile(COLUMN_FILE_NAME, fileHandle) != 0)
    {
        // If the table catalog hasn't been created
        _rbfm->createFile(TABLE_FILE_NAME);
        _rbfm->createFile(COLUMN_FILE_NAME);
        vector<Attribute> tableDesc;
        vector<Attribute> attributeDesc;
        getTableDescription(tableDesc);
        getAttributeDescription(attributeDesc);
        doCreateTable(TABLE_FILE_NAME, tableDesc, false);
        doCreateTable(COLUMN_FILE_NAME, attributeDesc, false);
    }else
    {
        _rbfm->closeFile(fileHandle);
    }
    if(initTableMap() != 0)
    {
        perror("rm constructor");
    }
}

RelationManager::~RelationManager()
{
    if(_rm)
    {
        delete _rm;
        _rm = 0;
    }
}

RC RelationManager::createTable(const string &tableName, const vector<Attribute> &attrs)
{
    return doCreateTable(tableName, attrs, true);
}

RC RelationManager::deleteTable(const string &tableName)
{
    // ----------------------
    // Delete table data file
    // ----------------------
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    int tableId = _tableMap[tableName];
    if(tableId == 0)
    {
        return -1;
    }
    if(_rbfm->destroyFile(tableName) != 0)
    {
        return -1;
    }
    // -----------------
    // Delete attributes
    // -----------------
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back("column-name");
    scan(COLUMN_FILE_NAME, "table-id", EQ_OP, &tableId, projectedAttr, rmsi);
    RID rid;
    char returnedData[MAX_TUPLE_SIZE];
    FileHandle fileHandle;
    vector<Attribute> attrs;
    if(getAttributes(tableName, attrs) != 0)
    {
        return -1;
    }
    if(_rbfm->openFile(COLUMN_FILE_NAME, fileHandle) != 0)
    {
        return -1;
    }
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        if(_rbfm->deleteRecord(fileHandle, attrs, rid) != 0)
        {
            return -1;
        }
    }
    _columnsMap.erase(tableId);
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    // ------------
    // Delete table
    // ------------
    RID tableRid;
    tableRid.pageNum = (tableId-1)/NUM_OF_SLOTS;
    tableRid.slotNum = (tableId-1)%NUM_OF_SLOTS;
    FileHandle tableFileHandle;
    if(_rbfm->openFile(TABLE_FILE_NAME, tableFileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> tableAttr;
    getTableDescription(tableAttr);
    if(_rbfm->deleteRecord(tableFileHandle, tableAttr, tableRid) != 0)
    {
        return -1;
    }
    _tableMap.erase(tableName);
    if(_rbfm->closeFile(tableFileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::getAttributes(const string &tableName, vector<Attribute> &attrs)
{
    return getAttributesSelectively(tableName, attrs, false);
}

RC RelationManager::insertTuple(const string &tableName, const void *data, RID &rid)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->insertRecord(fileHandle, attr, data, rid) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::deleteTuples(const string &tableName)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    if(_rbfm->deleteRecords(fileHandle) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::deleteTuple(const string &tableName, const RID &rid)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->deleteRecord(fileHandle, attr, rid) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::updateTuple(const string &tableName, const void *data, const RID &rid)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->updateRecord(fileHandle, attr, data, rid) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::readTuple(const string &tableName, const RID &rid, void *data)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->readRecord(fileHandle, attr, rid, data) != 0)
    {
        return -1;
    }
    if(removeNonExisted(tableName, data) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::readAttribute(const string &tableName, const RID &rid, const string &attributeName, void *data)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->readAttribute(fileHandle, attr, rid, attributeName, data) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::reorganizePage(const string &tableName, const unsigned pageNumber)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->reorganizePage(fileHandle, attr, pageNumber) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}

RC RelationManager::scan(const string &tableName,
      const string &conditionAttribute,
      const CompOp compOp,                  
      const void *value,                    
      const vector<string> &attributeNames,
      RM_ScanIterator &rm_ScanIterator)
{
    string contentFileName;
    getContentFileName(tableName, contentFileName, true);
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        perror("rm->scan: open file");
        return -1;
    }
    vector<Attribute> attr;
    if(contentFileName == TABLE_FILE_NAME)
    {
        getTableDescription(attr);
    }else if(contentFileName == COLUMN_FILE_NAME)
    {
        getAttributeDescription(attr);
    }else if(getAttributes(tableName, attr) != 0)
    {
        perror("rm->scan: getAttributes");
        return -1;
    }
    RBFM_ScanIterator* _rbfmsi = new RBFM_ScanIterator();
    if(_rbfm->scan(fileHandle, attr, conditionAttribute, compOp, value, attributeNames, *_rbfmsi) != 0)
    {
        perror("rm->scan: _rbfm->scan");
        return -1;
    }
    rm_ScanIterator.setRBFMSI(_rbfmsi);
    return 0;
}

RC RelationManager::initTableMap()
{
    RM_ScanIterator rmsi;
    vector<string> attrbuteNames;
    attrbuteNames.push_back("table-name");
    attrbuteNames.push_back("table-id");
    if(scan(TABLE_FILE_NAME, "", NO_OP, NULL, attrbuteNames, rmsi) != 0)
    {
        perror("initTableMap->scan");
        return -1;
    }
    RID rid;
    char scanData[MAX_TUPLE_SIZE];
    while(rmsi.getNextTuple(rid, scanData) != RM_EOF){
        //process the data;
        int stringLen;
        memcpy(&stringLen, scanData, sizeof(int));
        char tableNameChar[stringLen+1];
        memcpy(tableNameChar, scanData+sizeof(int), stringLen);
        tableNameChar[stringLen] = '\0';
        string tableName(tableNameChar);
        int tableId;
        memcpy(&tableId, scanData+sizeof(int)+stringLen, sizeof(int));
        _tableMap[tableName] = tableId;
    }
    rmsi.close();
    return 0;
}

RC RelationManager::getTableDescription(vector<Attribute> &recordDescriptor)
{
    Attribute tableId, tableName, fileName;
//    int tableFlag;
    tableId.name = "table-id";
    tableId.type = TypeInt;
    tableId.length = sizeof(int);
    recordDescriptor.push_back(tableId);
    tableName.name = "table-name";
    tableName.type = TypeVarChar;
    tableName.length = TABLE_NAME_LEN;
    recordDescriptor.push_back(tableName);
    fileName.name = "file-name";
    fileName.type = TypeVarChar;
    fileName.length = FILE_NAME_LEN;
    recordDescriptor.push_back(fileName);
    return 0;
}

RC RelationManager::getAttributeDescription(vector<Attribute> &recordDescriptor)
{
    Attribute descTableId, descName, descType, descLength, descColumnId, isExisted;
    recordDescriptor.clear();
    descTableId.name = "table-id";
    descTableId.type = TypeInt;
    descTableId.length = sizeof(int);
    recordDescriptor.push_back(descTableId);
    descName.name = "column-name";
    descName.type = TypeVarChar;
    descName.length = COLUMN_NAME_LEN;
    recordDescriptor.push_back(descName);
    descType.name = "column-type";
    descType.type = TypeInt;
    descType.length = sizeof(TypeInt);
    recordDescriptor.push_back(descType);
    descLength.name = "column-length";
    descLength.type = TypeInt;
    descLength.length = sizeof(int);
    recordDescriptor.push_back(descLength);
    descColumnId.name = "column-id";
    descColumnId.type = TypeInt;
    descColumnId.length = sizeof(int);
    recordDescriptor.push_back(descColumnId);
    isExisted.name = "is-existed";
    isExisted.type = TypeInt;
    isExisted.length = sizeof(int);
    recordDescriptor.push_back(isExisted);
    return 0;
}


RC RelationManager::getContentFileName(const string &tableName, string &fileName, bool isSystemTable)
{
    // Simple method, can be flexible
    if(!isSystemTable && checkTableName(tableName) != 0)
    {
        return -1;
    }
    fileName = tableName;
    return 0;
}

RC RelationManager::checkTableName(const string &tableName)
{
    if(tableName == TABLE_FILE_NAME || tableName == COLUMN_FILE_NAME)
    {
        return -1;
    }
    return 0;
}

RC  RelationManager::addNullField(const void *tupleData, void *recordData)
{
    vector<Attribute> attr;
    getAttributeDescription(attr);

    unsigned tupleOffset = 0;
    unsigned old_tupleOffset = 0;
    unsigned recordOffset = 0;
    for(unsigned int i=0; i<attr.size(); i++)
    {
        // THIS METHOD IS MODIFIED FROM RBFM_ScanIterator::retrieveRecord
        // if(attr[i].isExisted == 1)
        // This Condition is added to pass the compiling.
        if(1 == 1)
        {
            _rbfm->readAttributeData(tupleOffset, attr[i], tupleData, (char *)recordData + recordOffset);

            old_tupleOffset = tupleOffset;
            _rbfm->passOneAttribute(&tupleOffset, attr[i], tupleData);
            recordOffset += (tupleOffset - old_tupleOffset);
        }
        else
        {
            memset((char *)recordData + recordOffset, 0, sizeof(int));
            recordOffset += sizeof(int);
        }
    }

    return 0;
}

RC RelationManager::removeNonExisted(const string &tableName, void* data)
{
    int tableId = _tableMap[tableName];
    vector<Attribute> attr;
    getAttributesSelectively(tableName, attr, true);
    int leng = 0, reducedLeng = 0;
    leng = getSizeOfdata(attr, data);
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back("column-name");
    projectedAttr.push_back("column-type");
    projectedAttr.push_back("column-length");
    projectedAttr.push_back("column-id");
    projectedAttr.push_back("is-existed");
    scan(COLUMN_FILE_NAME, "table-id", EQ_OP, &tableId, projectedAttr, rmsi);
    RID rid;
    char returnedData[MAX_TUPLE_SIZE];
    std::map<int, Attribute> attrMap; // Arrange to get the correct order
    int totalOffset = 0;
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        Attribute attribute;
        int columnId, isExisted;
        AttrType columnType;
        AttrLength columnLength;
        int offset = 0, columnNameLength = 0;
        memcpy(&columnNameLength, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        char columnNameCharArray[columnNameLength+1];
        memcpy(&columnNameCharArray, returnedData+offset, columnNameLength);
        columnNameCharArray[columnNameLength] = '\0';
        string columnName(columnNameCharArray);
        offset += columnNameLength;
        memcpy(&columnType, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnLength, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnId, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&isExisted, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        if(isExisted == 1)
        {
            if(columnType == TypeVarChar)
            {
                int stringLen = 0;
                memcpy(&stringLen, (char*)data + totalOffset, sizeof(int));
                int AddtionOffset = stringLen + sizeof(int);
                totalOffset += AddtionOffset;
            } else
            {
                totalOffset += sizeof(int);
            }
        } else
        {
            if(columnType == TypeVarChar)
            {
                int stringLen = 0;
                memcpy(&stringLen, (char*)data + totalOffset, sizeof(int));
                int AddtionOffset = stringLen + sizeof(int);
                memcpy((char*)data + totalOffset, (char*)data + totalOffset + AddtionOffset, leng - totalOffset - reducedLeng - AddtionOffset);
                reducedLeng += AddtionOffset;
                memset((char*)data + leng - reducedLeng, 0, AddtionOffset);
                totalOffset += AddtionOffset;
            } else
            {
                memcpy((char*)data + totalOffset, (char*)data + totalOffset + sizeof(int), leng - totalOffset - reducedLeng - sizeof(int));
                reducedLeng += sizeof(int);
                memset((char*)data + leng - reducedLeng, 0, sizeof(int));
                totalOffset += sizeof(int);
            }
        }
    }
    return 0;
}

int RelationManager::getSizeOfdata(vector<Attribute> &attr, void* data)
{
    int offset = 0;
    for(int i = 0; i < (int)attr.size(); i++)
    {
        if(attr[i].type == TypeVarChar)
        {
            int stringLen = 0;
            memcpy(&stringLen, (char*)data + offset, sizeof(int));
            offset += stringLen;
            offset += sizeof(int);
        } else
        {
            offset += sizeof(int);
        }
    }
    return offset;
}
                          
RC RelationManager::getLastColumnIndex(const string &tableName, int &index)
{
    int tableId = _tableMap[tableName];
    if(tableId == 0)
    {
        perror("rm->getAttributes: tableId == 0");
        return -1;
    }
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back("column-name");
    projectedAttr.push_back("column-type");
    projectedAttr.push_back("column-length");
    projectedAttr.push_back("column-id");
    projectedAttr.push_back("is-existed");
    scan(COLUMN_FILE_NAME, "table-id", EQ_OP, &tableId, projectedAttr, rmsi);
    RID rid;
    char returnedData[MAX_TUPLE_SIZE];
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        Attribute attribute;
        int columnId, isExisted;
        AttrType columnType;
        AttrLength columnLength;
        int offset = 0, columnNameLength = 0;
        memcpy(&columnNameLength, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        char columnNameCharArray[columnNameLength+1];
        memcpy(&columnNameCharArray, returnedData+offset, columnNameLength);
        columnNameCharArray[columnNameLength] = '\0';
        string columnName(columnNameCharArray);
        offset += columnNameLength;
        memcpy(&columnType, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnLength, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnId, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&isExisted, returnedData+offset, sizeof(int));
        index = columnId;
    }
    rmsi.close();
    return 0;
}

RC RelationManager::getAttributesSelectively(const string &tableName, vector<Attribute> &attrs, bool needAll)
{
    if(tableName == TABLE_FILE_NAME)
    {
        return getTableDescription(attrs);
    }else if(tableName == COLUMN_FILE_NAME)
    {
        return getAttributeDescription(attrs);
    }
    int tableId = _tableMap[tableName];
    if(tableId == 0)
    {
        perror("rm->getAttributes: tableId == 0");
        return -1;
    }
    vector<Attribute> emptyAttr;
    if(memcmp(&_columnsMap[tableId], &emptyAttr, sizeof(vector<Attribute>)) != 0)
    {
        if(!needAll)
        {
            attrs = _columnsMap[tableId];
            return 0;
        }
    }
    // Search columns according to table id
    _columnsMap.erase(tableId);
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back("column-name");
    projectedAttr.push_back("column-type");
    projectedAttr.push_back("column-length");
    projectedAttr.push_back("column-id");
    projectedAttr.push_back("is-existed");
    scan(COLUMN_FILE_NAME, "table-id", EQ_OP, &tableId, projectedAttr, rmsi);
    RID rid;
    char returnedData[MAX_TUPLE_SIZE];
    std::map<int, Attribute> attrMap; // Arrange to get the correct order
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        Attribute attribute;
        int columnId, isExisted;
        AttrType columnType;
        AttrLength columnLength;
        int offset = 0, columnNameLength = 0;
        memcpy(&columnNameLength, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        char columnNameCharArray[columnNameLength+1];
        memcpy(&columnNameCharArray, returnedData+offset, columnNameLength);
        columnNameCharArray[columnNameLength] = '\0';
        string columnName(columnNameCharArray);
        offset += columnNameLength;
        memcpy(&columnType, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnLength, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnId, returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&isExisted, returnedData+offset, sizeof(int));
        if(isExisted == 0 && !needAll)
        {
            continue;
        }
        attribute.name = columnName;
        attribute.type = columnType;
        attribute.length = columnLength;
        attrMap[columnId] = attribute;
    }
    std::map<int, Attribute>::iterator it;
    for(it = attrMap.begin(); it != attrMap.end(); ++it)
    {
        attrs.push_back(it->second);
    }
    if(!needAll)
    {
        _columnsMap[tableId] = attrs;
    }
    rmsi.close();
    return 0;
}

RC RelationManager::doCreateTable(const string &tableName, const vector<Attribute> &attrs, bool fromOuter)
{
    // -----------------------
    // Fisrt create table info
    // -----------------------
    if(fromOuter && checkTableName(tableName) != 0)
    {
        return -1;
    }
    if(fromOuter && _tableMap[tableName] != 0)
    {
        return -1;
    }
    _tableMap.erase(tableName);
    
    vector<Attribute> tableInfo;
    getTableDescription(tableInfo);
    
    int dataLength = 0, tableNameLength = 0;
    tableNameLength = tableName.length();
    dataLength += 4 * sizeof(int);
    dataLength += 2 * tableNameLength;
    void* data = malloc(dataLength);;
    int offset = 0;
    //    int userFlag = 1;
    int tableId = 0;
    memset(data, 0, dataLength);
    memcpy((char *)data + offset, &tableId, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, &tableNameLength, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, tableName.c_str(), tableName.length());
    offset += tableName.length();
    memcpy((char *)data + offset, &tableNameLength, sizeof(int));
    offset += sizeof(int);
    memcpy((char *)data + offset, tableName.c_str(), tableName.length());
    offset += tableName.length();
    
    RID rid;
    FileHandle fileHandle;
    if(_rbfm->openFile(TABLE_FILE_NAME, fileHandle) != 0)
    {
        free(data);
        return -1;
    }
    if(_rbfm->insertRecord(fileHandle, tableInfo, data, rid) != 0)
    {
        perror("rm->createTable: insertRecord");
        free(data);
        return -1;
    }
    
    // table-id is calculated by rid
    int tableIdTrue = rid.pageNum * NUM_OF_SLOTS + rid.slotNum + 1;
    memcpy(data, &tableIdTrue, sizeof(int));
    if(_rbfm->updateRecord(fileHandle, tableInfo, data, rid) != 0)
    {
        perror("rm->createTable: updateRecord");
        free(data);
        return -1;
    }
    free(data);
    _tableMap[tableName] = tableIdTrue;
    
    // ----------------------
    // Create attributes info
    // ----------------------
    int columnIndexCount = 1;
    RID descRid;
    FileHandle columnFileHandle;
    if(_rbfm->openFile(COLUMN_FILE_NAME, columnFileHandle) != 0)
    {
        return -1;
    }
    for(vector<Attribute>::const_iterator it = attrs.begin(); it != attrs.end(); it++)
    {
        vector<Attribute> columnInfo;
        getAttributeDescription(columnInfo);
        
        int attrDataLength = 0, attrNameLength = 0, isExisted = 1;
        attrNameLength = it->name.length();
        attrDataLength = 6 * sizeof(int);
        attrDataLength += attrNameLength;
        void* attrData = malloc(attrDataLength);
        memset(attrData, 0, attrDataLength);
        int descOffset = 0;
        memcpy((char *)attrData + descOffset, &tableIdTrue, sizeof(int));
        descOffset += sizeof(int);
        memcpy((char *)attrData + descOffset, &attrNameLength, sizeof(int));
        descOffset += sizeof(int);
        memcpy((char *)attrData + descOffset, (it->name).c_str(), attrNameLength);
        descOffset += attrNameLength;
        memcpy((char *)attrData + descOffset, &(it->type), sizeof(TypeInt));
        descOffset += sizeof(TypeInt);
        memcpy((char *)attrData + descOffset, &(it->length), sizeof(int));
        descOffset += sizeof(int);
        memcpy((char *)attrData + descOffset, &columnIndexCount, sizeof(int));
        columnIndexCount++;
        descOffset += sizeof(int);
        memcpy((char *)attrData + descOffset, &isExisted, sizeof(int));
        
        if(_rbfm->insertRecord(columnFileHandle, columnInfo, attrData, descRid) != 0)
        {
            free(attrData);
            return -1;
        }
        free(attrData);
    }
    _columnsMap[tableIdTrue] = attrs;
    
    // Create data file
    if(_rbfm->createFile(tableName) != 0)
    {
        return -1;
    }
    return 0;
}
    
// Extra credit
RC RelationManager::dropAttribute(const string &tableName, const string &attributeName)
{
    int tableId = _tableMap[tableName];
    RM_ScanIterator rmsi;
    vector<string> projectedAttr;
    projectedAttr.push_back("table-id");
    projectedAttr.push_back("column-name");
    projectedAttr.push_back("column-type");
    projectedAttr.push_back("column-length");
    projectedAttr.push_back("column-id");
    projectedAttr.push_back("is-existed");
    scan(COLUMN_FILE_NAME, "table-id", EQ_OP, &tableId, projectedAttr, rmsi);
    RID rid;
    void* returnedData = malloc(MAX_TUPLE_SIZE);
    memset(returnedData, 0, MAX_TUPLE_SIZE);
    while(rmsi.getNextTuple(rid, returnedData) != RM_EOF)
    {
        Attribute attribute;
        int columnId, isExisted;
        AttrType columnType;
        AttrLength columnLength;
        int offset = 0, columnNameLength = 0, tableIdReturned = 0;
        memcpy(&tableIdReturned, (char*)returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnNameLength, (char*)returnedData+offset, sizeof(int));
        offset += sizeof(int);
        char columnNameCharArray[columnNameLength+1];
        memcpy(&columnNameCharArray, (char*)returnedData+offset, columnNameLength);
        columnNameCharArray[columnNameLength] = '\0';
        string columnName(columnNameCharArray);
        offset += columnNameLength;
        memcpy(&columnType, (char*)returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnLength, (char*)returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&columnId, (char*)returnedData+offset, sizeof(int));
        offset += sizeof(int);
        memcpy(&isExisted, (char*)returnedData+offset, sizeof(int));
        if(columnName == attributeName)
        {
            isExisted = 0;
            memcpy((char*)returnedData+offset, &isExisted, sizeof(int));
            string contentFileName = COLUMN_FILE_NAME;
            FileHandle fileHandle;
            if(_rbfm->openFile(contentFileName, fileHandle) != 0)
            {
                return -1;
            }
            vector<Attribute> attr;
            if(getAttributeDescription(attr) != 0)
            {
                return -1;
            }
            if(_rbfm->updateRecord(fileHandle, attr, returnedData, rid) != 0)
            {
                return -1;
            }
            if(_rbfm->closeFile(fileHandle) != 0)
            {
                return -1;
            }
            break;
        }
    }
    free(returnedData);
    _columnsMap.erase(tableId);
    return 0;
}

// Extra credit
RC RelationManager::addAttribute(const string &tableName, const Attribute &attr)
{
    int tableId = _tableMap[tableName];
    string contentFileName = COLUMN_FILE_NAME;
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attrDes;
    if(getAttributeDescription(attrDes) != 0)
    {
        return -1;
    }
    int attrDataLength = 0, attrNameLength = 0, isExisted = 1;
    attrNameLength = attr.name.length();
    attrDataLength = 6 * sizeof(int);
    attrDataLength += attrNameLength;
    void* attrData = malloc(attrDataLength);
    memset(attrData, 0, attrDataLength);
    int descOffset = 0;
    memcpy((char *)attrData + descOffset, &tableId, sizeof(int));
    descOffset += sizeof(int);
    memcpy((char *)attrData + descOffset, &attrNameLength, sizeof(int));
    descOffset += sizeof(int);
    memcpy((char *)attrData + descOffset, (attr.name).c_str(), sizeof(attr.name.length()));
    descOffset += attr.name.length();
    memcpy((char *)attrData + descOffset, &(attr.type), sizeof(TypeInt));
    descOffset += sizeof(TypeInt);
    memcpy((char *)attrData + descOffset, &(attr.length), sizeof(int));
    descOffset += sizeof(int);
    int lastIndex;
    getLastColumnIndex(tableName, lastIndex);
    lastIndex++;
    memcpy((char *)attrData + descOffset, &lastIndex, sizeof(int));
    descOffset += sizeof(int);
    memcpy((char *)attrData + descOffset, &isExisted, sizeof(int));
    RID rid;
    if(_rbfm->insertRecord(fileHandle, attrDes, attrData, rid) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    free(attrData);
    _columnsMap.erase(tableId);
    return 0;
}

// Extra credit
RC RelationManager::reorganizeTable(const string &tableName)
{
    string contentFileName;
    if(getContentFileName(tableName, contentFileName, false) != 0)
    {
        return -1;
    }
    FileHandle fileHandle;
    if(_rbfm->openFile(contentFileName, fileHandle) != 0)
    {
        return -1;
    }
    vector<Attribute> attr;
    if(getAttributes(tableName, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->reorganizeFile(fileHandle, attr) != 0)
    {
        return -1;
    }
    if(_rbfm->closeFile(fileHandle) != 0)
    {
        return -1;
    }
    return 0;
}


RC RM_ScanIterator::getNextTuple(RID &rid, void *data)
{
    if(_rbfmsi->getNextRecord(rid, data) != RBFM_EOF)
    {
        return 0;
    } else
    {
        return RM_EOF;
    }
}

RC RM_ScanIterator::close()
{
    if(_rbfmsi != NULL)
    {
        _rbfmsi->close();
        delete _rbfmsi;
        _rbfmsi = 0;
    }
    return 0;
}

RC RM_ScanIterator::setRBFMSI(RBFM_ScanIterator* _rbfmsi)
{
    this->_rbfmsi = _rbfmsi;
    return 0;
}